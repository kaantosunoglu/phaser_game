export default {
  gameWidth: 800,
  gameHeight: 450,
  localStorageName: 'phaseres6webpack',
  webfonts: ['Slackey', 'Open Sans']
}
