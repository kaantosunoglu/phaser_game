import 'pixi';
import 'p2';
import Phaser from 'phaser';

import BootState from './states/Boot';
import SplashState from './states/Splash';
import GameState from './states/Game';
import HomeState from './states/Home';
import WelcomeState from './states/Welcome';
import SelectMode from './states/SelectMode';
import Practice from './states/Practice';
import AvatarSelect from './states/AvatarSelect';
import Mission from './states/Mission';
import Animations from './states/Animations';
import GameOver from './states/GameOver';
import config from './config';


class Game extends Phaser.Game {
  constructor () {
    const docElement = document.documentElement;
    //const width = docElement.clientWidth > config.gameWidth ? config.gameWidth : docElement.clientWidth;
    //const height = docElement.clientHeight > config.gameHeight ? config.gameHeight : docElement.clientHeight;
    const width = 1920;
    const height = 1080;
    super(width, height, Phaser.CANVAS, 'content', null);
  //super(window.innerWidth, window.innerHeight, Phaser.CANVAS, 'content');

    this.state.add('Boot', BootState, false);
    this.state.add('Splash', SplashState, false);
    this.state.add('Game', GameState, false);
    this.state.add('Home', HomeState, false);
    this.state.add('Welcome', WelcomeState, false);
    this.state.add('SelectMode', SelectMode, false);
    this.state.add('Practice', Practice, false);
    this.state.add('AvatarSelect', AvatarSelect, false);
    this.state.add('Mission', Mission, false);
    //this.state.add('Animations', Animations, false);
    this.state.add('GameOver', GameOver, false);


    // with Cordova with need to wait that the device is ready so we will call the Boot state in another file
    if (!window.cordova) {
      this.state.start('Boot');
    }
  }
}

window.game = new Game()

if (window.cordova) {
  var app = {
    initialize: function () {
      document.addEventListener(
        'deviceready',
        this.onDeviceReady.bind(this),
        false
      )
    },

    // deviceready Event Handler
    //
    onDeviceReady: function () {
      this.receivedEvent('deviceready')

      // When the device is ready, start Phaser Boot state.
      window.game.state.start('Boot')
    },

    receivedEvent: function (id) {
     // console.log('Received Event: ' + id)
    }
  }

  app.initialize()
}
