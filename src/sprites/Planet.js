import Phaser from 'phaser'

export default class extends Phaser.Sprite {
    constructor({ game, x, y, asset, frameName }) {
        super(game, x, y, asset)
       //this.width *= 3;
       //this.height *= 3;
       this.animations.add('play', Phaser.Animation.generateFrameNames(frameName, 0, 63, '', 5), 12, true);
       this.animations.play('play');
    }

}
