import Phaser from 'phaser'

export default class extends Phaser.Sprite {
    constructor({ game, x, y, asset }) {
        super(game, x, y, asset)
       //this.width *= 3;
       //this.height *= 3;
       
       this.anchor.setTo(.5);
    }

    startAnimation() {
        //console.log("kaan 2");
        this.animations.add('hold', Phaser.Animation.generateFrameNames('Circle_Selected', 0, 29, '', 5), 12, false);
        this.animations.play('hold');
      }
    
      cancelAnimation() {
        this.animations.stop(null, true);
      }
}