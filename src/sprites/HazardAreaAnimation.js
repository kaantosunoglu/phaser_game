import Phaser from 'phaser'

export default class extends Phaser.Sprite {
    constructor({ game, x, y, asset }) {
        super(game, x, y, asset)
       //this.width *= 3;
       //this.height *= 3;
       this.animations.add('play', Phaser.Animation.generateFrameNames("Hazard_", 0, 24, '', 5), 12, true);
       this.animations.play('play');
    }

}
