import Phaser from 'phaser'

export default class extends Phaser.Sprite {
    constructor({ game, x, y, asset, point, physic }) {        
        let alienAnimation = asset + "animation"; 
        super(game, x, y, alienAnimation);
        this.asset = asset;
        this.velocityY = 0;
        this.alienType = asset;
        this.anchor.setTo(0.5);
        this.angle = (Math.random() - .5) * 90;
        this.width /= 3;
        this.height /= 3;
        this.physic = physic;
        this.point = point;
       // this.alive = false;

       if (this.physic) {
        this.game.physics.p2.enable(this);
        this.body.setCircle(40);
        this.body.fixedRotation = true;
        this.isDestroyed = false;
        this.isOntheGround = false; 
    }

        this.setAnimations();
        
    }

    setBody() {
        if(this.body == null) {
            
        }
    }
     
    setAnimations() {
       
        this.isDestroyed = false;
        this.isOntheGround = false;
        this.loadTexture(this.alienType + "animation");
        this.animations.add('fall', Phaser.Animation.generateFrameNames('Alien-Falling_', 0, 35, '', 5), 12, true);
        this.animations.play('fall');
    }
    destroyAnimation() { 
        if (!this.isDestroyed) {
            //this.isDestroyed = true;
            this.body.velocity.y = 0;
            this.loadTexture(this.alienType + "destroyed");
            let d = this.animations.add('destroy', Phaser.Animation.generateFrameNames('Alien-Destroyed_', 0, 15, '', 5), 12);
            d.play('destroy', 12, false, true);
            d.onComplete.add(() => this.isDestroyed = true, this);
            this.game.globals.score += this.point;
        }
    }
    onTheGround() {
        if (!this.isOntheGround) {
            this.isOntheGround = true;
            this.body.velocity.y = 0;
            this.angle = 0;
            this.loadTexture(this.alienType + "walk");
            this.animations.add('walk', Phaser.Animation.generateFrameNames('Alien-Walking_', 0, 35, '', 5), 12);
            this.animations.play('walk', 12, true, false);

            if (this.x < this.game.width / 2) {
                this.body.velocity.x = -(Math.random() + .5) * 20;
            } else {
                this.body.velocity.x = (Math.random() + .5) * 20;
            }
        }
    }

    update() {
        if (this.x < -50 || this.x > this.game.width + 50 || this.isDestroyed) {
            //this.alive = false;
            this.kill();
            //this.destroy();
        }
    }

    getVelocity() {
        return this.velocityY;
    }
}