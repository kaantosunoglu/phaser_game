import Phaser from 'phaser'

export default class extends Phaser.Sprite {
  constructor({ game, x, y, asset }) {
    super(game, x, y, asset)
    //this.anchor.setTo(0.5);
    //this.game.physics.arcade.enableBody(this);
    //this.body.immovable = true;

    this.width /= 2;
    this.height /= 2;



    this.animations.add('rotation', Phaser.Animation.generateFrameNames('Spaceship_', 0, 35, '', 5), 12, true);
    this.animations.play('rotation');
    this.game.physics.p2.enable(this, false);
    this.body.clearShapes();
    this.body.loadPolygon("shipphysics", "shipphysics");
    this.body.setZeroDamping();
    this.body.fixedRotation = true;

    this.body.kinematic = true;
    //this.body.setCircle(105);
    this.body.immovable = true;


  }
  update() {

  }
}
