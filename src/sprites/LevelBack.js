import Phaser from 'phaser'

export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset }) {
    super(game, x, y, asset); 
  //  this.anchor.setTo(0.5);
   }

   animate() {
    this.animations.add('play', Phaser.Animation.generateFrameNames('Animate', 0, 47, '', 5), 12, false);
    this.animations.play('play');
  }

}