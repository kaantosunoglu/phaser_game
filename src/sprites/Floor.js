import Phaser from 'phaser'

export default class extends Phaser.Sprite {
  constructor ({ game, x, y, asset }) {
    super(game, x, y, asset); 
    this.game.physics.p2.enable(this, false);
    this.body.setRectangleFromSprite(this);
    this.body.fixedRotation = true;
    this.body.kinematic = true;
  }
}