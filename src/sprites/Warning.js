import Phaser from 'phaser'

export default class extends Phaser.Sprite {
    constructor({ game, x, y, asset }) {
        super(game, x, y, asset)
       // this.width /= 2;
       // this.height /= 2;
        this.animations.add('play', Phaser.Animation.generateFrameNames('Warning_', 0, 23, '', 5), 12, true);
        this.animations.play('play');
    }
}
