import Phaser from 'phaser';
import HoldAnimation from '../assets/CursorHoldAnimation';

export default class extends Phaser.Group {
  constructor ({ game, x, y, asset }) {
    super(game)
    this.staticCursor = this.create(0, 0, 'cursor');
    //this.staticCursor.alpha = .5;
   //this.setAnimations();

   this.animatedCursor = new HoldAnimation({
    game:this.game,
    x: 0,
    y: 0,
    asset: 'whitecircleselected'
})

this.animatedCursor.x = ((this.animatedCursor.width - this.width) / 2) + 14 ;
this.animatedCursor.y = (this.animatedCursor.height - this.height) + 7;
this.add(this.animatedCursor);

    
  }
  startAnimation() {
    this.animatedCursor.startAnimation();
  }

  cancelAnimation() {
    this.animatedCursor.cancelAnimation();
  }

  loopAnimation() {
    this.animatedCursor.loopAnimation();
  }


  update(){
      // This sprite must follow the player hand 
      // Sync x & y with Hand x & y 
      this.x = this.game.globals.handX;
      this.y = this.game.globals.handY;
      this.x < 0 ? this.x = 0  : this.x;
      this.x > this.game.width - (this.width / 2) ? this.x = this.game.width - (this.width / 2) : this.x;
      this.y < 0 ? this.y = 0 : this.y;
      this.y > this.game.height - (this.height / 2) ? this.y = this.game.height - (this.height / 2) : this.y;

  }


}
