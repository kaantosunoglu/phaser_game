import Phaser from 'phaser'
export default class extends Phaser.Sprite {
    constructor({ game, x, y, asset }) {
        super(game, x, y, asset);
        //this.anchor.setTo(0.5);
        this.animations.add('play', Phaser.Animation.generateFrameNames('Character_', 0, 63, '', 5), 12, true);
        this.animations.play('play');
    }
}
