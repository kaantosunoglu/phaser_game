import Phaser from 'phaser';
import HoldAnimation from '../assets/CursorHoldAnimation';

export default class extends Phaser.Group {
  constructor ({ game, x, y, asset }) {
    super(game);
    this.staticCursor = this.create(0, 0, 'cursor');
    //this.staticCursor.alpha = .5;
   //this.setAnimations();

   this.animatedCursor = new HoldAnimation({
    game:this.game,
    x: 0,
    y: 0,
    asset: 'whitecircleselected'
})

this.animatedCursor.x = ((this.animatedCursor.width - this.width) / 2) + 14 ;
this.animatedCursor.y = (this.animatedCursor.height - this.height) + 7;
this.add(this.animatedCursor);

    
  }
  startAnimation() {
    this.animatedCursor.startAnimation();
  }

  cancelAnimation() {
    this.animatedCursor.cancelAnimation();
  }

  loopAnimation() {
    this.animatedCursor.loopAnimation();
  }

}
