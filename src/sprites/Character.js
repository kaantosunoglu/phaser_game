import Phaser from 'phaser';

export default class extends Phaser.Sprite {
  constructor({ game, x, y, asset, animate }) {
    super(game, x, y, asset);
    this.anchor.setTo(0.5);
   this.animate = animate;
    this.animations.add('play', Phaser.Animation.generateFrameNames('Character_', 0, 47, '', 5), 12, false);
    if(this.animate ) { 
      this.game.time.events.add(Phaser.Timer.SECOND * (Math.random() * 5), this.playAnimation, this);
    }
  }

  playAnimation() {
    this.animations.play('play');
    if(this.animate ) { 
      this.game.time.events.add(Phaser.Timer.SECOND * (Math.random() * 5) + 3, this.playAnimation, this);
    }
  }

}