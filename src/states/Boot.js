import Phaser from 'phaser'
import WebFont from 'webfontloader'
import config from '../config';


export default class extends Phaser.State {
  init() {
    this.stage.backgroundColor = '#390c39';
    this.fontsReady = false;
    this.fontsLoaded = this.fontsLoaded.bind(this);
    this.game.scale.trackParentInterval = 1;
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

  }

  preload() {
    game.time.advancedTiming = true;
    
    if (config.webfonts.length) {
      WebFont.load({
        google: {
          families: config.webfonts
        },
        active: this.fontsLoaded
      })
    }

    let text = this.add.text(this.world.centerX, this.world.centerY, 'loading fonts', { font: '16px Slackey', fill: '#dddddd', align: 'center' });
    text.anchor.setTo(0.5, 0.5);

    this.load.image('loaderBg', './assets/images/loader-bg.png');
    this.load.image('loaderBar', './assets/images/loader-bar.png');
    this.load.image('grey', './assets/images/grey.png');
    this.load.image('choose', './assets/images/UI/choose.png');
    this.load.image('cursor', './assets/images/UI/cursor/hand.png');
    this.load.image('lineswave', './assets/images/UI/cursor/linesWave.png');
    this.load.image('linesleftright', './assets/images/UI/cursor/linesLeftRight.png');
    //this.load.image('background', './assets/images/background.png');
    this.load.image('floor', './assets/images/floor.png');
    this.load.image('globalbackground', './assets/images/global_background.png');
    this.load.image('characterselect_background', './assets/images/characterselect_background.png');
    this.load.image('shiptrace', './assets/images/ship/shipTrace.png');
    this.load.image('bioverativlogo', './assets/images/UI/logo/bioverativLogo.png');
    this.load.image('sobilogo', './assets/images/UI/logo/sobiLogo.png');
    this.load.image('logoswhite', './assets/images/UI/logo/logosWhite.png');
    this.load.image('trialpath', './assets/images/trialPath.png');
    this.load.image('trialpath2', './assets/images/trialPath2.png');
    this.load.image('blueloading', './assets/images/blueLoading.png');
    this.load.image('lifeship', './assets/images/lifeship.png');
    this.load.image('timergreen', './assets/images/UI/timer/timer_green.png');
    this.load.image('timerorange', './assets/images/UI/timer/timer_orange.png');
    this.load.image('timerred', './assets/images/UI/timer/timer_red.png');

    // Character Animations
    this.load.atlas('Gus', './assets/images/characters/Gus.png', './assets/images/characters/Gus.json');
    this.load.atlas('Daisy', './assets/images/characters/Daisy.png', './assets/images/characters/Daisy.json');
    this.load.atlas('Leon', './assets/images/characters/Leon.png', './assets/images/characters/Leon.json');
    this.load.atlas('Marcus', './assets/images/characters/Marcus.png', './assets/images/characters/Marcus.json');
    this.load.atlas('Peter', './assets/images/characters/Peter.png', './assets/images/characters/Peter.json');
    this.load.atlas('GusSelected', './assets/images/characters/Gus_Selected.png', './assets/images/characters/Gus_Selected.json');
    this.load.atlas('DaisySelected', './assets/images/characters/Daisy_Selected.png', './assets/images/characters/Daisy_Selected.json');
    this.load.atlas('LeonSelected', './assets/images/characters/Leon_Selected.png', './assets/images/characters/Leon_Selected.json');
    this.load.atlas('MarcusSelected', './assets/images/characters/Marcus_Selected.png', './assets/images/characters/Marcus_Selected.json');
    this.load.atlas('PeterSelected', './assets/images/characters/Peter_Selected.png', './assets/images/characters/Peter_Selected.json');
    this.load.atlas('GusHead', './assets/images/characters/Gus-Head.png', './assets/images/characters/Gus-Head.json');
    this.load.atlas('DaisyHead', './assets/images/characters/Daisy-Head.png', './assets/images/characters/Daisy-Head.json');
    this.load.atlas('LeonHead', './assets/images/characters/Leon-Head.png', './assets/images/characters/Leon-Head.json');
    this.load.atlas('MarcusHead', './assets/images/characters/Marcus-Head.png', './assets/images/characters/Marcus-Head.json');
    this.load.atlas('PeterHead', './assets/images/characters/Peter-Head.png', './assets/images/characters/Peter-Head.json');
    this.load.atlas('GusScore', './assets/images/characters/Gus-Score.png', './assets/images/characters/Gus-Score.json');
    this.load.atlas('DaisyScore', './assets/images/characters/Daisy-Score.png', './assets/images/characters/Daisy-Score.json');
    this.load.atlas('LeonScore', './assets/images/characters/Leon-Score.png', './assets/images/characters/Leon-Score.json');
    this.load.atlas('MarcusScore', './assets/images/characters/Marcus-Score.png', './assets/images/characters/Marcus-Score.json');
    this.load.atlas('PeterScore', './assets/images/characters/Peter-Score.png', './assets/images/characters/Peter-Score.json');
    
    
    // Alien animations
    this.load.atlas('alien1animation', './assets/images/aliens/Alien1-falling.png', './assets/images/aliens/Alien1-falling.json');
    this.load.atlas('alien2animation', './assets/images/aliens/Alien2-falling.png', './assets/images/aliens/Alien2-falling.json');
    this.load.atlas('alien3animation', './assets/images/aliens/Alien3-falling.png', './assets/images/aliens/Alien3-falling.json');
    this.load.atlas('alien1destroyed', './assets/images/aliens/Alien-1_Destroyed.png', './assets/images/aliens/Alien-1_Destroyed.json');
    this.load.atlas('alien2destroyed', './assets/images/aliens/Alien-2_Destroyed.png', './assets/images/aliens/Alien-2_Destroyed.json');
    this.load.atlas('alien3destroyed', './assets/images/aliens/Alien-3_Destroyed.png', './assets/images/aliens/Alien-3_Destroyed.json');
    this.load.atlas('alien1walk', './assets/images/aliens/Alien1-Walking.png', './assets/images/aliens/Alien1-Walking.json');
    this.load.atlas('alien2walk', './assets/images/aliens/Alien2-Walking.png', './assets/images/aliens/Alien2-Walking.json');
    this.load.atlas('alien3walk', './assets/images/aliens/Alien3-Walking.png', './assets/images/aliens/Alien3-Walking.json');

    //UI Elements Animations
    this.load.atlas('titleanimation', './assets/images/UI/Protetion-Zone-TXT.png', './assets/images/UI/Protetion-Zone-TXT.json');
    this.load.atlas('circleselected', './assets/images/UI/Circle_Selected.png', './assets/images/UI/Circle_Selected.json');
    this.load.atlas('whitecircleselected', './assets/images/UI/White_Circle_Selected.png', './assets/images/UI/Circle_Selected.json');
    this.load.atlas('shipwiththecharacters', './assets/images/UI/Intro-Spaceship.png', './assets/images/UI/Intro-Spaceship.json');
   
    //Planet Animations
    this.load.atlas('planetA', './assets/images/UI/Planet-A.png', './assets/images/UI/Planet-A.json');
    this.load.atlas('planetB', './assets/images/UI/Planet-B.png', './assets/images/UI/Planet-B.json');
    this.load.atlas('planetC', './assets/images/UI/Planet-C.png', './assets/images/UI/Planet-C.json');
    this.load.atlas('planetD', './assets/images/UI/Planet-D.png', './assets/images/UI/Planet-D.json');
    this.load.atlas('hold', './assets/images/UI/Hold-Hand.png', './assets/images/UI/Hold-Hand.json');

    // Ship Physic
    this.load.atlas('ship', './assets/images/ship/Spaceship_Rotating.png', './assets/images/ship/Spaceship_Rotating.json');
    this.load.physics("shipphysics", "./assets/images/ship/spaceshipPhysics.json");

    //Level Backgrounds
    this.load.image('level1_1', './assets/images/levels/level1_1.png');
    this.load.image('level1_2', './assets/images/levels/level1_2.png');
    this.load.image('level1_3', './assets/images/levels/level1_3.png');
    this.load.image('zonelevel_1', './assets/images/levels/zonelevel1.png');
    this.load.image('level2_1', './assets/images/levels/level2_1.png');
    this.load.image('level2_2', './assets/images/levels/level2_2.png');
    this.load.image('level2_3', './assets/images/levels/level2_3.png');
    this.load.image('zonelevel_2', './assets/images/levels/zonelevel2.png');
    this.load.image('level3_1', './assets/images/levels/level3_1.png');
   // this.load.atlas('level3_2', './assets/images/levels/level3_2.png', './assets/images/levels/level3_2.json');
    this.load.image('level3_2', './assets/images/levels/level3_2.png');
    this.load.image('level3_3', './assets/images/levels/level3_3.png');
    this.load.image('zonelevel_3', './assets/images/levels/zonelevel3.png');


    this.load.atlas('hazard', './assets/images/Hazard.png', './assets/images/Hazard.json');


  }

  render() {



    if (config.webfonts.length && this.fontsReady) {
      this.state.start('Splash');
    }
    if (!config.webfonts.length) {
      this.state.start('Splash');
    }
  }

  fontsLoaded() {
    this.fontsReady = true;
  }

  update() {


  }
}
