import Phaser from 'phaser';
import Title from '../sprites/Title';
import ShipWithTheCharacters from '../sprites/ShipWithTheCharacters';
import CircleSelect from '../sprites/CircleSelect';
import Planet from '../sprites/Planet';
import Warning from '../sprites/Warning';

export default class extends Phaser.State {
    init() {
        var lastHandPositionX;
    }
    create() {
        this.titleImage = new Title({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'titleanimation'
        });
        this.titleImage.x = ((this.game.width - this.titleImage.width) / 2) - 100;
        this.titleImage.y = ((this.game.height - this.titleImage.height) / 2) - 200;
        this.game.add.existing(this.titleImage);

        this.ship = new ShipWithTheCharacters({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'shipwiththecharacters'
        });
        this.ship.x = ((this.game.width - this.ship.width) / 2) + 200;
        this.ship.y = ((this.game.height - this.ship.height) / 2) + 100;
       
        this.circle = new CircleSelect({
            game: this.game,
            x: 0,
            y: 50,
            asset: 'circleselected'
        });
        this.circle.x = ((this.game.width - this.ship.width) / 2) + 700;
        this.ship.y = ((this.game.height - this.ship.height) / 2) + 100;
        this.game.add.existing(this.circle);

        this.warning = new Warning({
            game: this.game,
            x: 222,
            y: this.game.height - 99,
            asset: 'warning'
        });
        this.game.add.existing(this.warning);

        this.planetA = new Planet({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'planetA',
            frameName: 'Planet_A_'
        });
        this.planetB = new Planet({
            game: this.game,
            x: 0,
            y: 200,
            asset: 'planetB',
            frameName: 'Planet_B_'
        });
        this.planetC = new Planet({
            game: this.game,
            x: 0,
            y: 400,
            asset: 'planetC',
            frameName: 'Planet_C_'
        });
        this.planetD = new Planet({
            game: this.game,
            x: 0,
            y: 700,
            asset: 'planetD',
            frameName: 'Planet_D_'
        });

        // this.ship.x = (this.game.width - this.ship.width) / 2;
        // this.ship.y = ((this.game.height - this.ship.height) / 2) + 100;
        this.game.add.existing(this.planetA);
        this.game.add.existing(this.planetB);
        this.game.add.existing(this.planetC);
        this.game.add.existing(this.planetD);

    }
}