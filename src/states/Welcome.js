import Phaser from 'phaser';
import HoldLight from '../sprites/HoldLight';
import { clone } from 'lodash';
import Cursor from '../sprites/Cursor';
import Planet from '../sprites/Planet';
import GlobalBackground from '../sprites/GlobalBackground';
import WaveAnimation from '../assets/CursorWaveAnimation';
import RightLeftAnimation from '../assets/CursorRightLeftAnimation';
import HoldAnimation from '../sprites/StaticCursor';
import StaticCursor from '../sprites/StaticCursor';

import NoMovementPopup from '../assets/NoMovement'

export default class extends Phaser.State {
    init() {
        var lastHandPositionX = 0;
        var isHolded = false;
        var holding = 0;
        var readyToSwipe = false;
        this.isHandAtTheRight = false;
        this.readyForNext = false;
        this.noMovement = 0;
        this.isNoMovementPopupOpen = false;
    }
    preload() { }

    create() {
        this.background = new GlobalBackground({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'globalbackground'
        });
        this.game.add.existing(this.background);

        this.setPlanets();
        this.setTexts();
        this.setHands();
        this.setFooter()

        this.cursor = new Cursor({
            game: this.game,
            x: this.world.centerX,
            y: this.world.centerY,
            asset: 'hold'
        });

        this.game.add.existing(this.cursor);


    }

    update() {
        
        if (!this.isNoMovementPopupOpen) {
            if (this.readyForNext && this.game.globals.handCount > 0) {
                this.noMovement = 0;
                if (this.game.globals.handX > 750) {
                    this.isHandAtTheRight = true;
                    this.readyToSwipe = true;
                }
                if (this.readyToSwipe && this.game.globals.handX < 600 && this.isHandAtTheRight) {
                    this.resetVariables();
                    this.state.start('SelectMode');
                }
            } else if (!this.game.globals.isKeyboardActive && this.game.globals.handCount <= 0) {
                this.noMovement++;
               // console.log(this.noMovement);
                if (this.noMovement >= 1800) {
                    this.noMovementPopup = new NoMovementPopup({ game: this.game });
                    this.game.add.existing(this.noMovementPopup);
                    this.isNoMovementPopupOpen = true;
                }
            }

            if (this.readyForNext && game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.resetVariables();
                this.state.start('SelectMode');
            }
        } else {
            if (this.noMovementPopup.quit) {
                this.noMovement = 0;

                this.state.start('Home');
                this.isNoMovementPopupOpen = false;
            } else if (this.noMovementPopup.resume) {
                this.noMovement = 0;
                this.noMovementPopup.destroy();
                this.isNoMovementPopupOpen = false;
            }
        }

    }
    render() { }

    setTexts() {
        let wellcomeText = this.add.text(0, 79, `Welcome to`, {
            font: '60px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        wellcomeText.x = (this.game.width - wellcomeText.width) / 2;
        wellcomeText.setShadow(-10, 10, 'rgba(0,0,0,1)', 0);
        wellcomeText.alpha = 0;


        let protectionText = this.add.text(0, 159, `PROTECTION ZONE`, {
            font: '80px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        protectionText.x = (this.game.width - protectionText.width) / 2;
        protectionText.setShadow(-10, 10, 'rgba(0,0,0,1)', 0);
        protectionText.alpha = 0;


        let infoText = this.add.text(0, 319, `This is a game you can play just by moving your hands`, {
            font: '42px Open Sans',
            fill: '#ffffff',
            align: 'center',
            smoothed: false
        });
        infoText.x = (this.game.width - infoText.width) / 2;
        infoText.alpha = 0;


        this.game.add.tween(wellcomeText).to({ alpha: 1 }, 1500, "Linear", true, 0, 0, false);
        this.game.add.tween(protectionText).to({ alpha: 1 }, 1500, "Linear", true, 1750, 0, false);
        this.game.add.tween(infoText).to({ alpha: 1 }, 1500, "Linear", true, 3500, 0, false);

    }

    setHands() {
        let waveAnim = new WaveAnimation({ game: this.game });
        game.add.existing(waveAnim);
        waveAnim.x = (this.game.width / 2) - 500;
        waveAnim.y = 520 + 100;
        waveAnim.alpha = 0;

        let moveAnim = new RightLeftAnimation({ game: this.game });
        this.game.add.existing(moveAnim);
        moveAnim.x = this.game.width / 2;
        moveAnim.y = 511;
        moveAnim.alpha = 0;

        let holdAnim = new StaticCursor({ game: this.game });
        this.game.add.existing(holdAnim);
        holdAnim.x = (this.game.width / 2) + 500;
        holdAnim.y = 451;
        holdAnim.alpha = 0;
        holdAnim.loopAnimation();

        let waveText = this.add.text(0, 685, `Wave\nyour hands`, {
            font: '34px Open Sans',
            fill: '#ffffff',
            align: 'center',
            smoothed: false
        });
        waveText.anchor.setTo(0.5);
        waveText.x = waveAnim.x;
        waveText.alpha = 0;

        let moveText = this.add.text(0, 685, `Move your hand\nleft and right`, {
            font: '34px Open Sans',
            fill: '#ffffff',
            align: 'center',
            smoothed: false
        });
        moveText.anchor.setTo(0.5);
        moveText.x = moveAnim.x;
        moveText.alpha = 0;

        let holdText = this.add.text(0, 685, `Hover over button\nto select`, {
            font: '34px Open Sans',
            fill: '#ffffff',
            align: 'center',
            smoothed: false
        });
        holdText.anchor.setTo(0.5);
        holdText.x = holdAnim.x + 50;
        holdText.alpha = 0;

        this.game.add.tween(waveAnim).to({ alpha: 1 }, 500, "Linear", true, 5500, 0, false);
        this.game.add.tween(waveText).to({ alpha: 1 }, 500, "Linear", true, 5500, 0, false);
        this.game.add.tween(moveAnim).to({ alpha: 1 }, 500, "Linear", true, 7000, 0, false);
        this.game.add.tween(moveText).to({ alpha: 1 }, 500, "Linear", true, 7000, 0, false);
        this.game.add.tween(holdAnim).to({ alpha: 1 }, 500, "Linear", true, 8500, 0, false);
        this.game.add.tween(holdText).to({ alpha: 1 }, 500, "Linear", true, 8500, 0, false);


    }

    setPlanets() {
        this.planetA = new Planet({
            game: this.game,
            x: 1330,
            y: 250,
            asset: 'planetA',
            frameName: 'Planet_A_'
        });
        this.planetB = new Planet({
            game: this.game,
            x: 60,
            y: 90,
            asset: 'planetB',
            frameName: 'Planet_B_'
        });
        this.planetC = new Planet({
            game: this.game,
            x: 1600,
            y: 700,
            asset: 'planetC',
            frameName: 'Planet_C_'
        });
        this.planetD = new Planet({
            game: this.game,
            x: -440,
            y: 480,
            asset: 'planetD',
            frameName: 'Planet_D_'
        });

        this.planetA.width *= 2.7;
        this.planetA.height *= 2.7;
        this.planetA.angle = -55;

        this.planetB.width /= 2;
        this.planetB.height /= 2;

        //this.planetC.width *= 1.3;
        //this.planetC.height *= 1.3;

        this.planetD.width *= 2;
        this.planetD.height *= 2;

        this.game.add.existing(this.planetA);
        this.game.add.existing(this.planetB);
        this.game.add.existing(this.planetC);
        this.game.add.existing(this.planetD);
    }

    setFooter() {
        let footerText = this.add.text(0, this.game.height - 60, 'Swipe your hand from right to left to continue', {
            font: '34px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        footerText.x = (this.game.width - footerText.width) / 2;
        footerText.y = this.game.height - footerText.height - 30;
        footerText.alpha = 0;

        let moveAnim = new RightLeftAnimation({ game: this.game });
        moveAnim.height /= 1.6;
        moveAnim.width /= 1.6;
        this.game.add.existing(moveAnim);
        moveAnim.x = this.game.width / 2;
        moveAnim.y = this.game.height - 120;
        moveAnim.alpha = 0;

        this.nextAnimation = this.game.add.tween(footerText).to({ alpha: 1 }, 1000, "Linear", true, 9000, 0, false);
        this.nextAnimation.onComplete.add(this.testForNext, this);
        this.game.add.tween(moveAnim).to({ alpha: 1 }, 1000, "Linear", true, 9000, 0, false);

    }

    testForNext() {
        this.readyForNext = true;
    }

    resetVariables() {
        var lastHandPositionX = 0;
        var isHolded = false;
        var holding = 0;
        var readyToSwipe = false;
        this.isHandAtTheRight = false;
        this.readyForNext = false;
        this.noMovement = 0;
        this.isNoMovementPopupOpen = false;
        this.nextAnimation = null;
        this.cursor.cancelAnimation();
    }
}
