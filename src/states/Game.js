import Phaser from 'phaser';
import Ship from '../sprites/SpaceShip';
import Background from '../sprites/Background';
import Floor from '../sprites/Floor';
import Alien from '../sprites/Alien';
import LostLife from '../assets/LostLife';
import Planet from '../sprites/Planet';
import LevelComplete from '../assets/LevelComplete'
import Cursor from '../sprites/Cursor';
import LevelBackground from '../assets/LevelBackground';
import GlobalBackground from '../sprites/GlobalBackground';
import CharacterGroup from '../sprites/Character';
import LifeShip from '../sprites/LifeShip';
import HazardAreaAnimation from '../sprites/HazardAreaAnimation';
import Blueloading from '../sprites/BlueLoading';
import Timer from '../sprites/Timer';
import NoMovementPopup from '../assets/NoMovement';

export default class extends Phaser.State {
  init() {
    this.game.globals.score = 0;
    this.game.globals.life = 5;
    var lastPosition;
    var isKeyboardSelected = false;
    this.hazardPlaying = false;
    this.alienInHazard = null;

    this.cursors = game.input.keyboard.createCursorKeys();
    this.timersSeconds = 30;
    this.firsAlienHitGround = false;
    this.isLostLifePopupOpened = false;
    this.isNextLevel = false;
    this.levelCompleteTimer = 3;
    this.phase = 0;
    this.aliensArray = [];
    this.levelCompletePopup = null;
    this.noMovement = 0;
    this.isNoMovementPopupOpen = false;
  }
  preload() { }

  create() {
    this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    this.setBackground();
    this.addPlanets();
    this.setTexts();
    this.lastPosition = 0;
    this.setCollisionGroups();
    //this.setUpAliens();
    this.summonAlien();
    this.game.time.events.add(Phaser.Timer.SECOND, this.updateTimer, this);

    this.hazardAreaAnimation = new HazardAreaAnimation({
      game: this.game,
      x: 0,
      y: this.hazardArea.body.y - 53,
      asset: 'hazard'
    });
    this.game.add.existing(this.hazardAreaAnimation);
    this.hazardAreaAnimation.visible = false;
  }

  render() {
    game.debug.geom(this, this.scoreText.textBounds, 'rgba(255,0,0,0.5)');
    game.debug.geom(this, this.timerText.textBounds, 'rgba(255,0,0,0.5)');
  }

  update() {
    if (!this.isNoMovementPopupOpen) {
      if (this.game.globals.handCount > 0) {
        this.noMovement = 0;
      } else if (!this.game.globals.isKeyboardActive && this.game.globals.handCount <= 0) {
        this.noMovement++;
        if (this.noMovement >= 1800) {
          this.game.physics.p2.pause();
          this.game.time.events.pause();
          this.game.time.gamePaused();
          this.noMovementPopup = new NoMovementPopup({ game: this.game });
          this.game.add.existing(this.noMovementPopup);
          this.isNoMovementPopupOpen = true;
        }
      }

      if (!this.isLostLifePopupOpened && !this.isNextLevel) {
        if (!this.isKeyboardSelected) {
          // Ship must move between screen bounds
          this.ship.body.x = this.game.globals.handX;
          this.ship.body.x = this.ship.body.x < this.protectionArea.x ? this.protectionArea.x : this.ship.body.x;
          this.ship.body.x = this.ship.body.x > (this.protectionArea.x + this.protectionArea.width) ? (this.protectionArea.x + this.protectionArea.width) : this.ship.body.x;

        } else {
          this.ship.body.setZeroVelocity();
        }
        if (this.cursors.left.isDown) {
          this.ship.body.moveLeft(800);
          this.ship.body.x = this.ship.body.x < 160 ? this.protectionArea.x : this.ship.body.x;
          this.ship.body.x = this.ship.body.x > (this.protectionArea.x + this.protectionArea.width) ? (this.protectionArea.x + this.protectionArea.width) : this.ship.body.x;
          this.isKeyboardSelected = true;
        }
        else if (this.cursors.right.isDown) {
          this.ship.body.moveRight(800);
          this.ship.body.x = this.ship.body.x < this.protectionArea.x ? this.protectionArea.x : this.ship.body.x;
          this.ship.body.x = this.ship.body.x > (this.protectionArea.x + this.protectionArea.width) ? (this.protectionArea.x + this.protectionArea.width) : this.ship.body.x;
          this.isKeyboardSelected = true;
        }

        if (this.game.globals.grabStrength == 1) {
          this.isKeyboardSelected = false;
        }

        // for ship move effecgt
        this.setShipAngle(this.ship.x, this.lastPosition);
        this.lastPosition = this.ship.x;
        this.scoreText.setText(this.game.globals.score);
        this.lifeText.setText(`x ${this.game.globals.life}`);
        if (this.game.globals.life <= 0) {
          // GAME OVER
          this.gameOver();
        }
      } else {

        if (this.levelCompletePopup != null && this.levelCompletePopup.changeState) {
          this.game.globals.level = 1;
          this.game.globals.score = 0;
          this.game.globals.life = 5;
          this.clearPool();
          this.state.start('Home');
        }

        this.ship.body.moveLeft(0);
        if (this.isLostLifePopupOpened || this.levelCompletePopup.readyForNext) {
          if (!this.isKeyboardSelected) {
            if (this.game.globals.handCount > 0) {
              if (this.game.globals.handX > 750) {
                this.isHandAtTheRight = true;
                this.readyToSwipe = true;
              }
              if (this.readyToSwipe && this.game.globals.handX < 600 && this.isHandAtTheRight) {
                if (this.game.globals.handX > 750) {
                  this.isHandAtTheRight = true;
                }
                if (this.game.globals.handX < 600 && this.isHandAtTheRight) {
                  this.isHandAtTheRight = false;
                  // this.cursor.visible = false;
                  this.holding = 0;
                  this.readyToSwipe = false;
                  this.isHolded = false;

                  if (this.isLostLifePopupOpened) {
                    //this.closeLostLifePopup();
                  } else {

                    if (this.phase == 0) {
                      this.phase++;
                      this.levelCompletePopup.levelInfo({ level: this.game.globals.level });
                    } else if (this.phase == 1) {
                      if (this.game.globals.level < 3) {
                        this.phase++;
                        this.levelCompleteTimer = 3;
                        this.startNewLevel();
                        this.levelCompletePopup.setShip({ level: this.game.globals.level });
                      } else {
                        //console.log("high score control ")
                        this.phase = 3;
                        this.levelCompletePopup.setSelfieScreen();
                      }
                    } else if (this.phase == 3) {
                      this.game.globals.level = 1;
                      this.game.globals.score = 0;
                      this.game.globals.life = 5;
                      this.clearPool();
                      this.state.start('Home');
                    }
                  }
                }
              }
            }
            else {
              this.holding = 0;
              this.readyToSwipe = false;
              this.isHolded = false;

            }
          } else {
            if (this.spaceKey.downDuration(1)) {
              if (this.isLostLifePopupOpened) {
                // this.closeLostLifePopup();
              } else {

                if (this.phase == 0) {
                  this.phase++;
                  this.levelCompletePopup.levelInfo({ level: this.game.globals.level });
                } else if (this.phase == 1) {
                  if (this.game.globals.level < 3) {
                    this.phase++;
                    this.levelCompleteTimer = 3;
                    this.startNewLevel();
                    this.levelCompletePopup.setShip({ level: this.game.globals.level });
                  } else {
                    this.phase = 3;
                    this.levelCompletePopup.setSelfieScreen();
                  }
                } else if (this.phase == 3) {
                  this.game.globals.level = 1;
                  this.game.globals.score = 0;
                  this.game.globals.life = 5;
                  this.clearPool();
                  this.state.start('Home');
                }
              }
            }
          }
        }

      }
      this.levelBackground.updateBackground({ shipPositionX: this.ship.x })
    } else {
      if (this.noMovementPopup.quit) {
        this.hazardAreaAnimation.visible = false;
        this.game.globals.level = 1;
        this.game.globals.score = 0;
        this.game.globals.life = 5;
        this.aliens.killAll();
        this.game.physics.p2.resume();
        this.game.time.events.resume();
        this.noMovement = 0;
        this.clearPool();
        this.state.start('Home');
        this.isNoMovementPopupOpen = false;
      } else if (this.noMovementPopup.resume) {
        this.game.physics.p2.resume();
        this.game.time.events.resume();
        this.game.time.gameResumed();
        this.noMovement = 0;
        this.noMovementPopup.destroy();
        this.isNoMovementPopupOpen = false;
      }
    }
  }

  startNewLevel() {
    this.levelCompletePopup.changeLevel({ countdownTime: this.levelCompleteTimer });
    this.levelCompleteTimer--;
    if (this.levelCompleteTimer >= 0) {
      this.game.time.events.add(Phaser.Timer.SECOND, this.startNewLevel, this);
    } else {
      this.levelCompletePopup.tweenShip({ level: this.game.globals.level });
      this.setNewLevelTween = game.add.tween(this).to({}, 1009, "Back.easeInOut", true, 0, 0, false);
      this.setNewLevelTween.onComplete.add(this.setNewLevel, this);

    }
  }

  setNewLevel() {
    //start
    this.levelCompletePopup.kill();
    this.isNextLevel = false;
    this.phase = 0;
    this.game.globals.level++;
    this.timersSeconds = 30;

    this.levelBackground.changeBackground({ level: this.game.globals.level });
    this.summonAlien();
    this.updateTimer();
    this.setPlanets();
  }

  alienHitShip(ship, alien) {
    if (alien == this.alienInHazard) {
      this.hazardPlaying = false;
      this.hazardAreaAnimation.visible = false;
      this.hazardArea.body.y = this.game.height - (this.hazardArea.height / 2) - 120;
      this.alienInHazard = null;
    }
    alien.kinematic = true;
    alien.velocity.x = alien.velocity.y = 0;
    alien.sprite.destroyAnimation();
  }

  alienHitFloor(ground, alien) {

    this.game.globals.life--;
    alien.kinematic = true;
    alien.velocity.x = alien.velocity.y = 0;
    alien.sprite.onTheGround();
    let tweenW = this.lifeText.width * 1.8;
    let tweenH = this.lifeText.height * 1.8;
    let tweenX = this.lifeText.x - ((tweenW - this.lifeText.width) / 2);
    let tweenY = this.lifeText.y - ((tweenH - this.lifeText.height) / 2);
    this.resumeGame = this.game.add.tween(this.lifeText).from({ width: tweenW, height: tweenH, x: tweenX, y: tweenY }, 250, "Linear", true, 0, 0, false);

    // play protection zone animation

    if (this.firsAlienHitGround == false) {
      // kill all aliens
      this.aliens.killAll();
      // stop timer and aliens
      this.game.time.events.pause();
      this.game.time.gamePaused();
      // get you lost life screen
      this.lostLifePopup = new LostLife({
        game: this.game
      });
      this.isLostLifePopupOpened = true;
      this.firsAlienHitGround = true;
      this.resumeGame = this.game.add.tween(this).to({}, 3000, "Back.easeInOut", true, 0, 0, false);
      this.resumeGame.onComplete.add(this.closeLostLifePopup, this);
      this.hazardAreaAnimation.visible = false;
    }
    if (this.game.globals.life <= 0) {
      this.game.globals.life = 0;
      if (this.game.globals.highScore < this.game.score) {
        // well done?
        this.game.globals.highScore = this.game.score;
      }
    }
    this.hazardPlaying = false;
    this.hazardAreaAnimation.visible = false;
    this.hazardArea.body.y = this.game.height - (this.hazardArea.height / 2) - 120;
    this.alienInHazard = null;

  }

  alienHitHazard(hazard, alien) {
    //console.log(alien.velocity.y + "   alienHitHazard  1  " + alien.sprite.velocityY);
    alien.velocity.y = alien.sprite.velocityY / 2;
    if (!this.hazardPlaying) {

      this.hazardArea.body.y = 10000;
      //console.log("alienHitHazard 2");
      this.alienInHazard = alien;
      this.hazardPlaying = true;
      this.hazardAreaAnimation.visible = true;
    }
  }

  gameOver() {
    this.hazardAreaAnimation.visible = false;
    this.game.globals.level = 1;
    this.game.globals.score = 0;
    this.game.globals.life = 5;
    this.aliens.killAll();
    this.clearPool();
    this.state.start('GameOver');
  }

  clearPool() {
    for (let t1 = 0; t1 < this.game.globals.type1Aliens.length; t1++) {
      this.game.globals.type1Aliens[t1] = null;
    }
    for (let t2 = 0; t2 < this.game.globals.type2Aliens.length; t2++) {
      this.game.globals.type2Aliens[t2] = null;
    }
    for (let t3 = 0; t3 < this.game.globals.type3Aliens.length; t3++) {
      this.game.globals.type3Aliens[t3] = null;
    }
    this.game.globals.type1Aliens = [];
    this.game.globals.type2Aliens = [];
    this.game.globals.type3Aliens = [];
  }

  setShipAngle(shipPosition, lastPosition) {

    // I just want to give ship movement effect smootly
    if (shipPosition - lastPosition >= -1 && shipPosition - lastPosition <= 1) {
      if (this.ship.angle >= .5 && this.ship.angle <= .5) {
        this.ship.body.angle = this.ship.angle = 0;

      } else {
        this.ship.body.angle += (this.ship.angle / -5);
        this.ship.angle += (this.ship.angle / -5);
      }
    } else if (shipPosition - lastPosition < -1) {
      if (this.ship.angle > 0) {
        this.ship.body.angle += (this.ship.angle < .4) ? (this.ship.angle / -1) : (this.ship.angle / -5);
        this.ship.angle += (this.ship.angle < .4) ? (this.ship.angle / -1) : (this.ship.angle / -5);
      } else {
        this.ship.body.angle += (-20 < this.ship.angle) ? -7 : 0;
        this.ship.angle += (-20 < this.ship.angle) ? -7 : 0;
      }
    } else {
      if (this.ship.angle < 0) {
        this.ship.body.angle += (this.ship.angle > -0.4) ? (this.ship.angle / -1) : (this.ship.angle / -5);
        this.ship.angle += (this.ship.angle > -0.4) ? (this.ship.angle / -1) : (this.ship.angle / -5);
      } else {
        this.ship.body.angle += (20 > this.ship.angle) ? 7 : 0;
        this.ship.angle += (20 > this.ship.angle) ? 7 : 0;
      }
    }
  }

  setUpAliens() {
    // call summonAlien function in random time later
    //let s = ((Phaser.Timer.SECOND / (this.game.globals.level)) * Math.random());
    let s = (Phaser.Timer.SECOND * Math.random()) + (Phaser.Timer.SECOND / (this.game.globals.level * 0.5));
    //console.log("S : " + s);
    this.game.time.events.add(s, this.summonAlien, this);
  }

  summonAlien() {
    //create alien with randoms
    if (!this.isNextLevel) {

      if (this.aliensArray.length == 0)
        this.aliensArray = this.game.globals.aliens.slice();

      let alienMode = Math.floor(Math.random() * (this.aliensArray.length));
      let alienX = (Math.random() * (this.game.width - 400)) + 200;
      let alienPoint = (this.aliensArray[alienMode] == 1) ? 5 : ((this.aliensArray[alienMode] == 2) ? 3 : 2);

      let alienFromPool = this.getAlienFromPool(this.aliensArray[alienMode]);
      if (alienFromPool == null) {
        this.alien = new Alien({
          game: this.game,
          x: alienX,
          y: 0,
          asset: 'alien' + this.aliensArray[alienMode],
          point: alienPoint,
          physic: true
        });
        //this.alien.alive = true;
        this.aliens.add(this.alien);

        switch (this.aliensArray[alienMode]) {
          case 1:
            this.game.globals.type1Aliens.push(this.alien);
            break;
          case 2:
            this.game.globals.type2Aliens.push(this.alien);
            break;
          case 3:
            this.game.globals.type3Aliens.push(this.alien);
            break;
          default:
            break;
        }
      } else {
        this.alien = alienFromPool;
        this.alien.reset(alienX, 0);
        if (this.alien.body == null) {
          this.alien.setBody();
        }
        this.alien.body.x = alienX;
        this.alien.body.y = 0;
        this.alien.body.dynamic = true;
        // this.alien.alive = true;
        this.alien.setAnimations();
      }



      this.alien.body.setCollisionGroup(this.alienCollisionGroup);
      this.alien.body.collides([this.shipCollisionGroup, this.floorCollisionGroup, this.hazardCollisionGroup]);

      this.aliensArray.splice(alienMode, 1);

      this.alien.velocityY = this.alien.body.velocity.y = (Math.random() * 100) + (70 + (30 * this.game.globals.level));

      this.setUpAliens();
    }
  }

  getAlienFromPool(alienMode) {
    let alienInThePool = null;
    if (alienMode == 1) {
      if (this.game.globals.type1Aliens.length > 0) {
        for (let i = 0; i < this.game.globals.type1Aliens.length; i++) {
          if (!this.game.globals.type1Aliens[i].alive) {
            alienInThePool = this.game.globals.type1Aliens[i];
          }
        }
      }
    } else if (alienMode == 2) {
      if (this.game.globals.type2Aliens.length > 0) {
        for (let i = 0; i < this.game.globals.type2Aliens.length; i++) {
          if (!this.game.globals.type2Aliens[i].alive) {
            alienInThePool = this.game.globals.type2Aliens[i];
          }
        }
      }
    } else if (alienMode == 3) {
      if (this.game.globals.type3Aliens.length > 0) {
        for (let i = 0; i < this.game.globals.type3Aliens.length; i++) {
          if (!this.game.globals.type3Aliens[i].alive) {
            alienInThePool = this.game.globals.type3Aliens[i];
          }
        }
      }
    }

    return alienInThePool;

  }

  updateTimer() {
    if (!this.isNextLevel) {
      if (this.timersSeconds == 30) {
        this.timerBackground.loadTexture('timergreen');
      } else if (this.timersSeconds == 15) {
        this.timerBackground.loadTexture('timerorange');
      } else if (this.timersSeconds == 5) {
        this.timerBackground.loadTexture('timerred');
      }

      this.timerText.setText(this.timersSeconds);
      this.timersSeconds--;

      if (this.timersSeconds < 0 && this.game.globals.life > 0) {
        this.aliens.killAll();
        if (this.game.globals.level === 3) {
          if (localStorage.getItem('highscore') === null) {
            localStorage.setItem('highscore', this.game.globals.score);
            this.game.globals.highScore = this.game.globals.score;
          } else if (this.game.globals.score > localStorage.getItem('highscore')) {
            localStorage.setItem('highscore', this.game.globals.score);
            this.game.globals.highScore = this.game.globals.score;
          }
        }

        this.isNextLevel = true;
        this.hazardAreaAnimation.visible = false;
        this.levelCompletePopup = new LevelComplete({ game: this.game });
        this.game.add.existing(this.levelCompletePopup);

        //this.levelCompletePopup.visible = false;
        this.levelCompletePopup.levelComplete({ level: this.game.globals.level });
        //set new level parameters
      } else {
        this.game.time.events.add(Phaser.Timer.SECOND, this.updateTimer, this);
      }
    }
  }

  closeLostLifePopup() {
    this.lostLifePopup.kill();
    this.isLostLifePopupOpened = false;
    this.game.time.events.resume();
    this.game.time.gameResumed();
  }

  setBackground() {
    this.background = new GlobalBackground({
      game: this.game,
      x: 0,
      y: 0,
      asset: 'globalbackground'
    });
    this.game.add.existing(this.background);
    this.levelBackground = new LevelBackground({ game: this.game, level: this.game.globals.level });
    this.game.add.existing(this.levelBackground);
    this.levelBackground.x = (this.game.width - this.levelBackground.width) / 2;
    this.levelBackground.y = this.game.height - this.levelBackground.height;
  }

  setCollisionGroups() {
    this.protectionArea = this.game.add.graphics(0, 0);
    this.protectionArea.lineStyle(0);
    this.protectionArea.beginFill(0xffa201, 0.4);
    this.protectionArea.drawRect(0, 0, 1580, 220);
    this.protectionArea.endFill();
    this.protectionArea.x = (this.game.width - this.protectionArea.width) / 2;
    this.protectionArea.y = this.game.height - 220;
    this.protectionArea.visible = false;

    this.ship = new Ship({
      game: this.game,
      x: this.game.width / 2,
      y: this.game.height - 220,
      asset: 'ship'
    });
    this.ship.angle = 0;
    this.game.add.existing(this.ship);
    this.shipCollisionGroup = this.physics.p2.createCollisionGroup();
    this.ship.body.setCollisionGroup(this.shipCollisionGroup);

    this.ground = new Floor({
      game: this.game,
      x: this.game.width / 2,
      y: 0,
      asset: 'level1_1'
    });

    this.game.add.existing(this.ground);
    this.ground.alpha = 0;
    this.ground.body.y = this.game.height - (this.ground.height / 2) + 10;
    this.floorCollisionGroup = this.physics.p2.createCollisionGroup();
    this.ground.body.setCollisionGroup(this.floorCollisionGroup);

    this.hazardArea = new Floor({
      game: this.game,
      x: this.game.width / 2,
      y: 0,
      asset: 'level1_1'
    });
    this.game.add.existing(this.hazardArea);
    this.hazardArea.alpha = 0;
    this.hazardArea.body.y = this.game.height - (this.hazardArea.height / 2) - 120;
    this.hazardCollisionGroup = this.physics.p2.createCollisionGroup();
    this.hazardArea.body.setCollisionGroup(this.hazardCollisionGroup);

    // "aliens" is group for aliens.
    // We won't try to dedect to if alien hit ship. We'll try dedect if group element hit the ship
    this.aliens = this.game.add.physicsGroup(Phaser.Physics.P2JS);
    this.alienCollisionGroup = this.physics.p2.createCollisionGroup();


    this.ship.body.collides(this.alienCollisionGroup, this.alienHitShip, this);
    this.ground.body.collides(this.alienCollisionGroup, this.alienHitFloor, this);
    this.hazardArea.body.collides(this.alienCollisionGroup, this.alienHitHazard, this);
  }

  addPlanets() {
    this.planetA = new Planet({
      game: this.game,
      x: 0,
      y: 0,
      asset: 'planetA',
      frameName: 'Planet_A_'
    });
    this.planetB = new Planet({
      game: this.game,
      x: 0,
      y: 0,
      asset: 'planetB',
      frameName: 'Planet_B_'
    });
    this.planetC = new Planet({
      game: this.game,
      x: 0,
      y: 0,
      asset: 'planetC',
      frameName: 'Planet_C_'
    });
    this.planetD = new Planet({
      game: this.game,
      x: 0,
      y: 0,
      asset: 'planetD',
      frameName: 'Planet_D_'
    });
    this.game.add.existing(this.planetA);
    this.game.add.existing(this.planetB);
    this.game.add.existing(this.planetC);
    this.game.add.existing(this.planetD);

    this.setPlanets();
  }

  setPlanets() {


    switch (this.game.globals.level) {
      case 1:
        this.planetA.width *= .7;
        this.planetA.height *= .7;
        this.planetA.x = -130;
        this.planetA.y = 365;
        this.planetA.alpha = .5;

        this.planetB.width *= .3;
        this.planetB.height *= .3;
        this.planetB.y = 135;
        this.planetB.alpha = .5;

        this.planetC.width *= 1.2;
        this.planetC.height *= 1.2;
        this.planetC.x = 1200;
        this.planetC.y = -150;
        this.planetC.alpha = .5;
        //this.planetC.visible = false;

        this.planetD.width *= 1.6;
        this.planetD.height *= 1.6;
        this.planetD.visible = false;
        break;
      case 2:
        this.planetA.width *= .7;
        this.planetA.height *= .7;
        this.planetA.x = 1745;
        this.planetA.y = 255;
        this.planetA.alpha = .5;

        this.planetB.width *= .3;
        this.planetB.height *= .3;
        this.planetB.y = 135;
        this.planetB.alpha = .5;
        this.planetB.visible = false;

        this.planetC.width *= .9;
        this.planetC.height *= .9;
        this.planetC.x = -150;
        this.planetC.y = 1;
        this.planetC.alpha = .5;
        //this.planetC.visible = false;

        this.planetD.width *= .4;
        this.planetD.height *= .4;
        this.planetD.x = 800;
        this.planetD.y = -100;
        this.planetD.alpha = .5;
        this.planetD.visible = true;
        break;
      case 3:
        this.planetA.width *= .7;
        this.planetA.height *= .7;
        this.planetA.x = 1745;
        this.planetA.y = 255;
        this.planetA.alpha = .5;
        this.planetA.visible = false;

        this.planetB.width *= .6;
        this.planetB.height *= .6;
        this.planetB.y = 400;
        this.planetB.x = -105;
        this.planetB.alpha = .5;
        this.planetB.visible = true;

        this.planetC.width *= 1.2;
        this.planetC.height *= 1.2;
        this.planetC.x = 150;
        this.planetC.y = -180;
        this.planetC.alpha = .5;
        this.planetC.visible = true;

        this.planetD.width *= .4;
        this.planetD.height *= .4;
        this.planetD.x = 1720;
        this.planetD.y = 500;
        this.planetD.alpha = .5;
        this.planetD.visible = true;
        break;

      default:
        break;
    }


  }

  setTexts() {
    this.selectedCharacter = new CharacterGroup({
      game: this.game,
      x: 0,
      y: 0,
      asset: this.game.globals.characterNames[this.game.globals.character] + "Score",
      animate: true
    });
    this.selectedCharacter.width *= 1.4;
    this.selectedCharacter.height *= 1.4;
    this.selectedCharacter.x = 1550 + (this.selectedCharacter.width / 2);
    this.selectedCharacter.y = 20 + (this.selectedCharacter.height / 2);

    this.game.add.existing(this.selectedCharacter);

    this.scoreText = this.add.text(0, 0, 0, {
      font: '46px Slackey',
      fill: '#fff1da',
      smoothed: false,
      align: 'right',
      boundsAlignH: "right",
    });
    this.scoreText.setTextBounds(this.selectedCharacter.x - 90, this.selectedCharacter.y - 35, 100, 268);
    this.scoreText.setShadow(-4, 4, 'rgba(0,0,0,1)', 0);


    this.timerBackground = new Timer({
      game: this.game,
      x: 50,
      y: 85,
      asset: 'timergreen'
    });
    this.game.add.existing(this.timerBackground);


    this.timerText = this.add.text(0, 0, this.timersSeconds, {
      font: '40px Slackey',
      fill: '#ffffff',
      smoothed: false,
      align: 'right',
      boundsAlignH: "right",
    });
    this.timerText.setTextBounds(this.timerBackground.x, 98, 170, 268);
    this.timerText.setShadow(-4, 4, 'rgba(0,0,0,1)', 0);


    this.lifeShip = new LifeShip({
      game: this.game,
      x: 60,
      y: this.game.height - 90,
      asset: 'lifeship'
    });

    this.game.add.existing(this.lifeShip);

    this.lifeText = this.add.text(200, this.world.height - 90, `x ${this.game.globals.life}`, {
      font: '48px Slackey',
      fill: '#fff1da',
      smoothed: false
    })
    this.lifeText.setShadow(-4, 4, 'rgba(0,0,0,1)', 0);
  }
}
