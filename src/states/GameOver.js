import Phaser from 'phaser';
import CharacterGroup from '../assets/CharacterGroup';
import Cursor from '../sprites/Cursor';
import ChooseButton from '../assets/ChooseButton';
import Helper from '../globals/Helpers';


export default class extends Phaser.State {
    init() {
        this.helper = new Helper();
        this.hitTime = 0;
    }
    preload() { }

    create() {

        this.setTexts();

        this.setButtons();
       


        let footerText = this.add.text(0, this.game.height - 60, 'Hold your hand over the button you wish to choose', {
            font: '34px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        footerText.x = (this.game.width - footerText.width) / 2;


        this.cursor = new Cursor({
            game: this.game,
            x: this.world.centerX,
            y: this.world.centerY,
            asset: 'hold'
        });

        this.game.add.existing(this.cursor);
    }

    update() {
        if (this.game.globals.handCount > 0) {
            if (this.helper.checkOverlap(this.retryButton, this.cursor)) {
                if(this.hitTime == 1) {
                    this.retryButton.startAnimation();
                }
                this.hitTime++;
                if (this.hitTime === 155) {
                    this.retryGame();
                }
            } else if (this.helper.checkOverlap(this.quitButton, this.cursor)) {
                if(this.hitTime == 1) {
                    this.quitButton.startAnimation();
                }
                this.hitTime++;
                if (this.hitTime === 155) {
                    this.quitGame();
                }
            }
            else {
                this.retryButton.stopAnimation();
                this.quitButton.stopAnimation();
                this.hitTime = 0;
            }
        } else {
            this.retryButton.stopAnimation();
                this.quitButton.stopAnimation();
            this.hitTime = 0;
        }
        
        if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
             this.quitGame();
        }
        if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
            this.retryGame();
        }


    }

    setTexts(){
        this.headerText = this.add.text(0, 160, `OH NO!`, {
            font: '80px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        this.headerText.x = (this.game.width - this.headerText.width) / 2;
        this.headerText.setShadow(-10, 10, 'rgba(0,0,0,0.5)', 0);

        this.infoText = this.add.text(0, 320, `That’s your last turn gone!`, {
            font: '42px Open Sans',
            fill: '#ffffff',
            align: 'center',
            smoothed: false
        });
        this.infoText.x = (this.game.width - this.infoText.width) / 2;

        this.gameOverText = this.add.text(0, 450, `GAME OVER`, {
            font: '100px Slackey',
            fill: '#53c7f1',
            align: 'center',
            smoothed: false
        });
        this.gameOverText.x = (this.game.width - this.gameOverText.width) / 2;
        this.gameOverText.setShadow(-14, 14, 'rgba(0,0,0,0.5)', 0);

    }

    setButtons(){
        this.quitButton = new ChooseButton({
            game: this.game,
            buttonText: 'QUIT'
        });
        //this.quitButton.width = 225;
        //this.quitButton.height = 225;
        this.quitButton.y = 645; 
        this.quitButton.x = ((this.game.width - this.quitButton.width) / 2) - 400;

        this.retryButton = new ChooseButton({
            game: this.game,
            buttonText: 'RETRY'
        });
        //this.retryButton.width = 225;
        //this.retryButton.height = 225;
        this.retryButton.y = 645;
        this.retryButton.x = ((this.game.width - this.retryButton.width) / 2) + 400;

        this.game.add.existing(this.quitButton);
        this.game.add.existing(this.retryButton);
    }

    quitGame() {
        this.state.start('Home');
    }

    retryGame() {
        this.state.start('AvatarSelect');
    }

}