import Phaser from 'phaser';
import { centerGameObjects } from '../utils';
import globals from '../globals/Global';
import { clone } from 'lodash';
import LeapController from '../assets/LeapController';

export default class extends Phaser.State {
  init() { }

  preload() {
    this.loaderBg = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBg')
    this.loaderBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBar')
    centerGameObjects([this.loaderBg, this.loaderBar])

    this.game.globals = clone(globals);
    //console.log(this.game.globals);
    this.load.setPreloadSprite(this.loaderBar)

    if (localStorage.getItem('highscore') === null) {
    }else{
      this.game.globals.highScore = localStorage.getItem('highscore');
    }


    this.lc = new LeapController(
      {
        game: this.game,
        x: this.world.centerX,
        y: this.world.centerY,
        asset: 'leapcontroller'
      }
    )

    this.game.globals.leapcontroller = this.lc;

    //
    // load your assets
    //


  }

  create() {
    this.game.physics.startSystem(Phaser.Physics.P2JS);
    this.game.physics.p2.setImpactEvents(true);

    this.state.start('Home');
    //this.state.start('Game');
  }
}
