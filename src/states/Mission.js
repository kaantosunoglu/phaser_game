import Phaser from 'phaser';
import CharacterGroup from '../sprites/Character';
import Cursor from '../sprites/Cursor';
import GlobalBackground from '../sprites/GlobalBackground';
import Planet from '../sprites/Planet';
import Alien from '../sprites/Alien';
import RightLeftAnimation from '../assets/CursorRightLeftAnimation';
import ShipWithTheCharacters from '../sprites/ShipWithTheCharacters';
import ShipTrace from '../sprites/ShipTrace';
import Blueloading from '../sprites/BlueLoading';

import NoMovementPopup from '../assets/NoMovement'

export default class extends Phaser.State {
    init() {
        this.readyForNext = false;

        this.noMovement = 0;
        this.isNoMovementPopupOpen = false;
    }
    preload() { }

    create() {
        this.countdown = 4;
        this.isCountdownStart = false;

        this.background = new GlobalBackground({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'globalbackground'
        });
        this.game.add.existing(this.background);

        this.setPlanets();
        this.setTexts();

        this.setCharacterImage();
        this.setAliens();
        this.setFooter();

        this.cursor = new Cursor({
            game: this.game,
            x: this.world.centerX,
            y: this.world.centerY,
            asset: 'hold'
        });

        this.game.add.existing(this.cursor);
    }

    update() {

        if (!this.isNoMovementPopupOpen) {

            if (this.readyForNext) {
                if (this.game.globals.handCount > 0 && this.countdown === 4) {
                        this.noMovement = 0;
                    if (this.game.globals.handX > 750) {
                        this.isHandAtTheRight = true;
                        this.readyToSwipe = true;
                    }
                    if (this.readyToSwipe && this.game.globals.handX < 600 && this.isHandAtTheRight) {
                        this.isHandAtTheRight = false;
                        this.holding = 0;
                        this.readyToSwipe = false;
                        this.isHolded = false;
                        this.cursor.cancelAnimation();
                        this.isCountdownStart = true;
                        this.changeMissionScreen();
                    }
                } else if (!this.game.globals.isKeyboardActive && this.game.globals.handCount <= 0) {
                    this.noMovement++;
                    if (this.noMovement >= 1800) {
                        this.noMovementPopup = new NoMovementPopup({ game: this.game });
                        this.game.add.existing(this.noMovementPopup);
                        this.isNoMovementPopupOpen = true;
                    }
                }
                else {
                    this.isHandAtTheRight = false;
                    this.holding = 0;
                    this.readyToSwipe = false;
                    this.isHolded = false;
                    this.cursor.cancelAnimation();
                }
                if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.countdown === 4) {
                    this.game.globals.isKeyboardActive = true;
                    this.isCountdownStart = true;
                    this.changeMissionScreen();
                }
            }
        } else {
            if (this.noMovementPopup.quit) {
                this.noMovement = 0;                
                this.state.start('Home');
                this.isNoMovementPopupOpen = false;
            } else if (this.noMovementPopup.resume) {
                this.noMovement = 0;
                this.noMovementPopup.destroy();
                this.isNoMovementPopupOpen = false;
            }
        }
    }

    setPlanets() {
        this.planetA = new Planet({
            game: this.game,
            x: 1330,
            y: 250,
            asset: 'planetA',
            frameName: 'Planet_A_'
        });
        this.planetA.visible = false;
        this.planetB = new Planet({
            game: this.game,
            x: 1580,
            y: -140,
            asset: 'planetB',
            frameName: 'Planet_B_'
        });
        this.planetC = new Planet({
            game: this.game,
            x: 1650,
            y: 850,
            asset: 'planetC',
            frameName: 'Planet_C_'
        });
        this.planetD = new Planet({
            game: this.game,
            x: 0,
            y: 450,
            asset: 'planetD',
            frameName: 'Planet_D_'
        });

        this.planetA.width *= 2.7;
        this.planetA.height *= 2.7;
        this.planetA.angle = -55;

        // this.planetB.width *= 1.4;
        // this.planetB.height *= 1.4;

        this.planetC.width *= .7;
        this.planetC.height *= .7;

        this.planetD.width *= .7;
        this.planetD.height *= .7;

        this.game.add.existing(this.planetA);
        this.game.add.existing(this.planetB);
        this.game.add.existing(this.planetC);
        this.game.add.existing(this.planetD);
    }

    setTexts() {
        this.headerText = this.add.text(0, 0, 'YOUR MISSION', {
            font: '80px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        this.headerText.y = 80;
        this.headerText.x = (this.game.width - this.headerText.width) / 2;
        this.headerText.setShadow(-10, 10, 'rgba(0,0,0,1)', 0);
        this.headerText.alpha = 0;
        this.game.add.tween(this.headerText).to({ alpha: 1 }, 500, "Linear", true, 0, 0, false);


        this.levelText = this.add.text(0, 160, 'LEVEL 1', {
            font: '60px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        this.levelText.y = 160;
        this.levelText.x = (this.game.width - this.levelText.width) / 2;
        this.levelText.setShadow(-10, 10, 'rgba(0,0,0,1)', 0);
        this.levelText.visible = false;


        this.infoText = this.add.text(0, 220, 'Your mission is to protect your planet\nfrom alien invasion. As the aliens fall from the sky,\nmove your spaceship right and left to attack and destroy\nthem before they can enter the protection zone', {
            font: '42px Open Sans',
            fill: '#ffffff',
            align: 'center',
            smoothed: false
        });
        this.infoText.x = (this.game.width - this.infoText.width) / 2;
        this.infoText.alpha = 0;
        this.game.add.tween(this.infoText).to({ alpha: 1 }, 1500, "Linear", true, 600, 0, false);

        this.descText = this.add.text(0, 810, `Reach 30 points to progress to the next level`, {
            font: '46px Slackey',
            fill: '#53c7f1',
            align: 'center',
            smoothed: false,
            boundsAlignH: "center",
            boundsAlignV: "bottom"
        });
        this.descText.x = (this.game.width - this.descText.width) / 2;
        this.descText.padding.y = 33;
        this.descText.setShadow(-7, 7, 'rgba(0,0,0,1)', 0);
        this.descText.visible = false;

        this.niceWorkText = this.add.text(0, 180, ` NICE WORK!`, {
            font: '120px Slackey',
            fill: '#53c7f1',
            align: 'center',
            smoothed: false,
            boundsAlignH: "center",
            boundsAlignV: "bottom"
        });
        this.niceWorkText.x = (this.game.width - this.niceWorkText.width) / 2;
        this.niceWorkText.y = (this.game.height - this.niceWorkText.height) / 2;
        this.niceWorkText.padding.y = 33;
        this.niceWorkText.setShadow(-20, 20, 'rgba(0,0,0,1)', 0);
        this.niceWorkText.visible = false;

        this.countdownText = this.add.text(0, 0, this.countdown, {
            font: '144px Slackey',
            fill: '#53c7f1',
            align: 'center',
            smoothed: false
        });
        this.niceWorkText.setShadow(-10, 10, 'rgba(0,0,0,1)', 0);
        this.countdownText.x = (this.game.width - this.countdownText.width) / 2;
        this.countdownText.y = (this.game.height - this.countdownText.height) / 2;
        this.countdownText.visible = false;
    }

    setCharacterImage() {
        this.selectedCharacter = new CharacterGroup({
            game: this.game,
            x: 380,
            y: 140,
            asset: this.game.globals.characterNames[this.game.globals.character] + "Head",
            animate: true
        });
        this.selectedCharacter.width = 230;
        this.selectedCharacter.height = 230;
        this.game.add.existing(this.selectedCharacter);
        //this.game.time.events.add(Phaser.Timer.SECOND * (Math.random() * 5), this.playCharacterAnimation, this);
    }

    setAliens() {
        this.alien1 = new Alien({
            game: this.game,
            x: 0,
            y: 0,
            asset: "alien1"
        });
        this.game.add.existing(this.alien1);
        this.alien1.angle = 0;
        this.alien1.height *= 1.5;
        this.alien1.width *= 1.5;
        this.alien1.x = (this.alien1.width / 2) + 500;
        this.alien1.y = (this.alien1.height / 2) + 541;
        this.alien1Text = this.add.text(0, 160, '5 points', {
            font: '34px Open Sans',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        this.alien1Text.y = this.alien1.y + 100;
        this.alien1Text.x = (this.alien1.x - (this.alien1.width / 2)) + ((this.alien1.width - this.alien1Text.width) / 2);

        this.alien2 = new Alien({
            game: this.game,
            x: 0,
            y: 0,
            asset: "alien2"
        });
        this.game.add.existing(this.alien2);
        this.alien2.angle = 0;
        this.alien2.height *= 2;
        this.alien2.width *= 2;
        this.alien2.x = (this.alien2.width / 2) + 860;
        this.alien2.y = (this.alien2.height / 2) + 515;
        this.alien2Text = this.add.text(0, 160, '3 points', {
            font: '34px Open Sans',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        this.alien2Text.y = this.alien2.y + 100;
        this.alien2Text.x = (this.alien2.x - (this.alien2.width / 2)) + ((this.alien2.width - this.alien2Text.width) / 2);

        this.alien3 = new Alien({
            game: this.game,
            x: 0,
            y: 0,
            asset: "alien3"
        });
        this.game.add.existing(this.alien3);
        this.alien3.angle = 0;
        this.alien3.height *= 2;
        this.alien3.width *= 2;
        this.alien3.x = (this.alien3.width / 2) + 1250;
        this.alien3.y = (this.alien3.height / 2) + 485;
        this.alien3Text = this.add.text(0, 160, '2 points', {
            font: '34px Open Sans',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        this.alien3Text.y = this.alien3.y + 120;
        this.alien3Text.x = (this.alien3.x - (this.alien3.width / 2)) + ((this.alien3.width - this.alien3Text.width) / 2);

        this.alien1.alpha = 0;
        this.alien1Text.alpha = 0;
        this.alien2.alpha = 0;
        this.alien2Text.alpha = 0;
        this.alien3.alpha = 0;
        this.alien3Text.alpha = 0;

        this.game.add.tween(this.alien1).to({ alpha: 1 }, 500, "Linear", true, 3500, 0, false);
        this.game.add.tween(this.alien2).to({ alpha: 1 }, 500, "Linear", true, 4000, 0, false);
        this.game.add.tween(this.alien3).to({ alpha: 1 }, 500, "Linear", true, 4500, 0, false);

        this.game.add.tween(this.alien1Text).to({ alpha: 1 }, 500, "Linear", true, 3500, 0, false);
        this.game.add.tween(this.alien2Text).to({ alpha: 1 }, 500, "Linear", true, 4000, 0, false);
        this.game.add.tween(this.alien3Text).to({ alpha: 1 }, 500, "Linear", true, 4500, 0, false);
    }

    setFooter() {
        this.footerText = this.add.text(0, this.game.height - 60, 'Swipe your hand from right to left to continue', {
            font: '34px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        this.footerText.x = (this.game.width - this.footerText.width) / 2;
        this.footerText.y = this.game.height - this.footerText.height - 30;
        this.footerText.alpha = 0;
        this.nextAnimation = this.game.add.tween(this.footerText).to({ alpha: 1 }, 1000, "Linear", true, 5500, 0, false);
        this.nextAnimation.onComplete.add(() => this.readyForNext = true, this);

        this.moveAnim = new RightLeftAnimation({ game: this.game });
        this.moveAnim.height /= 1.6;
        this.moveAnim.width /= 1.6;
        this.game.add.existing(this.moveAnim);
        this.moveAnim.x = this.game.width / 2;
        this.moveAnim.y = this.game.height - 120;
        this.moveAnim.alpha = 0;
        this.game.add.tween(this.moveAnim).to({ alpha: 1 }, 1000, "Linear", true, 5500, 0, false);
    }

    // playCharacterAnimation() {
    //     this.selectedCharacter.playAnimation();
    //     this.game.time.events.add(Phaser.Timer.SECOND * (Math.random() * 30), this.playCharacterAnimation, this);;
    // }
    changeMissionScreen() {
        if (this.countdown === 4) {
            this.selectedCharacter.kill();

            this.planetA.visible = true;
            this.planetA.width /= 1.5;
            this.planetA.height /= 1.5;
            this.planetA.x = 1220;
            this.planetA.y = 966;

            this.planetB.width *= .3;
            this.planetB.height *= .3;
            this.planetB.x = 1755;
            this.planetB.y = 300;

            this.planetC.width *= 2.1;
            this.planetC.height *= 2.1;
            this.planetC.x = -25;
            this.planetC.y = 40;

            this.planetD.width *= 1.2;
            this.planetD.height *= 1.2;
            this.planetD.x = 1400;
            this.planetD.y = 10;

            this.headerText.setText("READY TO START IN");
            this.headerText.y = 280;
            this.headerText.x = (this.game.width - this.headerText.width) / 2;

            this.loading = new Blueloading({
                game: this.game,
                x: 0,
                y: 0,
                asset: "blueloading"
            });
            this.game.add.existing(this.loading);
            this.loading.x = ((this.game.width - this.loading.width) / 2) + (this.loading.width / 2);
            this.loading.y = ((this.game.height - this.loading.height) / 2) + (this.loading.height / 2) + 10;

            this.levelText.visible = true;
            this.infoText.kill();
            this.footerText.kill();
            this.countdownText.visible = true;
            this.descText.kill();
            this.alien1.kill();
            this.alien2.kill();
            this.alien3.kill();
            this.alien1Text.kill();
            this.alien2Text.kill();
            this.alien3Text.kill();
            this.moveAnim.kill();
            this.countdown--;
            this.countdownText.setText(this.countdown > 0 ? this.countdown : "GO!");
            this.setShip();
            this.timerEvent = this.game.time.events.add(Phaser.Timer.SECOND, this.changeMissionScreen, this);
        } else if (this.countdown > 1) {
            this.countdown--;

            this.countdownText.setText(this.countdown > 0 ? this.countdown : "GO!");
            this.countdownText.x = (this.game.width - this.countdownText.width) / 2;
            this.timerEvent = this.game.time.events.add(Phaser.Timer.SECOND, this.changeMissionScreen, this);
        } else {
            this.loading.kill();
            this.countdown--;
            this.countdownText.setText(this.countdown > 0 ? this.countdown : "GO!");
            this.countdownText.x = (this.game.width - this.countdownText.width) / 2;
            this.tweenShip();
        }
    }

    tweenShip() {
        this.shipTween.stop();
        this.shipTraceTween.stop();
        this.shipTraceTween = game.add.tween(this.shipTrace).to({ y: 450, x: 2500 }, 1000, "Back.easeInOut", true, 0, 0, false);
        this.shipTween = game.add.tween(this.ship).to({ y: 200, x: 3500 }, 1000, "Back.easeInOut", true, 0, 0, false);
        this.shipTween.onComplete.add(this.changeState, this);
    }

    changeState() {
        this.readyForNext = false;
        this.noMovement = 0;
        this.isNoMovementPopupOpen = false;
        this.countdown = 4;
        this.isCountdownStart = false;
        this.state.start('Game');
    }

    setShip() {
        this.shipTrace = new ShipTrace({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'shiptrace'
        })
        this.shipTrace.x = -745;
        this.shipTrace.y = 863;
        this.game.add.existing(this.shipTrace);

        this.ship = new ShipWithTheCharacters({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'shipwiththecharacters'
        });
        this.ship.width *= 1.5;
        this.ship.height *= 1.5;
        this.ship.x = -80;
        this.ship.y = 450;
        var shipTargetX = this.ship.x - 30;
        var shipTargetY = this.ship.y + 30;
        this.game.add.existing(this.ship);
        this.shipTween = game.add.tween(this.ship).to({ y: shipTargetY, x: shipTargetX }, 2000, "Quad.easeInOut", true, 0, -1, true);

        var traceTargetX = this.shipTrace.x - 30;
        var traceTargetY = this.shipTrace.y + 30;
        this.shipTraceTween = game.add.tween(this.shipTrace).to({ y: traceTargetY, x: traceTargetX, alpha: .5 }, 2000, "Quad.easeInOut", true, 0, -1, true);

    }

    shutdown() {
        this.countdown = 4;
    }
}