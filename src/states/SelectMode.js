import Phaser from 'phaser';
import ChooseButton from '../assets/ChooseButton';
import Cursor from '../sprites/Cursor';
import Helpers from '../globals/Helpers';
import GlobalBackground from '../sprites/GlobalBackground';
import StaticCursor from '../sprites/StaticCursor';
import Planet from '../sprites/Planet';
import NoMovementPopup from '../assets/NoMovement'

export default class extends Phaser.State {

    init() {
        var hitTime = 0;
        this.helper = new Helpers();
        this.noMovement = 0;
        this.isNoMovementPopupOpen = false;
    }

    create() {
        this.background = new GlobalBackground({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'globalbackground'
        });
        this.game.add.existing(this.background);

        this.setPlanets();
        this.setTexts();
        this.setButtons();
        this.setFooter();


        this.cursor = new Cursor({
            game: this.game,
            x: this.world.centerX,
            y: this.world.centerY,
            asset: 'hold'
        });

        this.game.add.existing(this.cursor);
    }


    update() {
      if (!this.isNoMovementPopupOpen) {

          if (this.game.globals.handCount > 0) {
              this.noMovement = 0;
          } else if (!this.game.globals.isKeyboardActive && this.game.globals.handCount <= 0) {
              this.noMovement++;
              if (this.noMovement >= 1800) {
                  this.noMovementPopup = new NoMovementPopup({ game: this.game });
                  this.game.add.existing(this.noMovementPopup);
                  this.isNoMovementPopupOpen = true;
              }
          }

            if (this.game.globals.handCount) {
                if (this.helper.checkOverlap(this.playButton, this.cursor)) {
                    this.hitTime++;
                    if (this.hitTime === 1) {
                        this.playButton.startAnimation();
                    }
                    if (this.hitTime >= ((this.game.time.fps * 155) / 60)) {
                        this.state.start("AvatarSelect");
                        //this.changeScale();
                    }
                } else if (this.helper.checkOverlap(this.practiceButton, this.cursor)) {
                    this.hitTime++;
                    if (this.hitTime === 1) {
                        this.practiceButton.startAnimation();
                    }
                    if (this.hitTime >= ((this.game.time.fps * 155) / 60)) {
                        this.state.start("Practice");
                        //this.changeScale();
                    }
                }
                else {
                    this.practiceButton.stopAnimation();
                    this.playButton.stopAnimation();
                    this.hitTime = 0;
                }
            } 

            if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
                //console.log('pressedLeft')
                this.state.start('Practice')
                this.game.globals.isKeyboardActive = true;
            }
            if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                //console.log('pressedRight')
                this.state.start("AvatarSelect")
                this.game.globals.isKeyboardActive = true;
            }
        } else {
            if (this.noMovementPopup.quit) {
                this.noMovement = 0;
                this.state.start('Home');
                this.isNoMovementPopupOpen = false;
            } else if (this.noMovementPopup.resume) {
                this.noMovement = 0;
                this.noMovementPopup.destroy();
                this.isNoMovementPopupOpen = false;
            }
        }
    }

    setPlanets() {
        this.planetA = new Planet({
            game: this.game,
            x: 1330,
            y: 250,
            asset: 'planetA',
            frameName: 'Planet_A_'
        });
        this.planetB = new Planet({
            game: this.game,
            x: -90,
            y: 231,
            asset: 'planetB',
            frameName: 'Planet_B_'
        });
        this.planetC = new Planet({
            game: this.game,
            x: -200,
            y: -353,
            asset: 'planetC',
            frameName: 'Planet_C_'
        });
        this.planetD = new Planet({
            game: this.game,
            x: 1590,
            y: 500,
            asset: 'planetD',
            frameName: 'Planet_D_'
        });

        this.planetA.width *= 2.7;
        this.planetA.height *= 2.7;
        this.planetA.angle = -55;

        this.planetB.width /= 1.8;
        this.planetB.height /= 1.8;

        this.planetC.width *= 2.3;
        this.planetC.height *= 2.3;

        this.planetD.width *= 2;
        this.planetD.height *= 2;

        // this.game.add.existing(this.planetA);
        this.game.add.existing(this.planetB);
        this.game.add.existing(this.planetC);
        this.game.add.existing(this.planetD);
    }

    setTexts() {
        let headerText = this.add.text(0, 160, `PLEASE CHOOSE`, {
            font: '80px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        headerText.x = (this.game.width - headerText.width) / 2;
        headerText.setShadow(-10, 10, 'rgba(0,0,0,0.5)', 0);
        let infoText = this.add.text(0, 320, `You can have a practice run or get straight into it`, {
            font: '42px Open Sans',
            fill: '#ffffff',
            align: 'center',
            smoothed: false
        });
        infoText.x = (this.game.width - infoText.width) / 2;
    }

    setButtons() {
        this.practiceButton = new ChooseButton({
            game: this.game,
            buttonText: 'PRACTICE\nMAKES\nPERFECT'
        });
        this.practiceButton.y = 525;
        this.practiceButton.x = ((this.game.width - this.practiceButton.width) / 2) - 400;

        this.playButton = new ChooseButton({
            game: this.game,
            buttonText: 'I\'M\nREADY\nTO GO'
        });
        this.playButton.y = 525;
        this.playButton.x = ((this.game.width - this.playButton.width) / 2) + 400;

        this.game.add.existing(this.practiceButton);
        this.game.add.existing(this.playButton);
    }

    setFooter() {
        let footerText = this.add.text(0, this.game.height - 60, 'Hold your hand over the button you wish to choose', {
            font: '34px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        footerText.x = (this.game.width - footerText.width) / 2;
        let holdAnim = new StaticCursor({ game: this.game });
        holdAnim.width /= 1.6;
        holdAnim.height /= 1.6;
        this.game.add.existing(holdAnim);
        holdAnim.x = (this.game.width / 2);
        holdAnim.y = this.game.height - 160;
        holdAnim.loopAnimation();
    }


    // checkOverlap(spriteA, spriteB) {

    //     var boundsA = spriteA.getBounds();
    //     var boundsB = spriteB.getBounds();

    //     return Phaser.Rectangle.intersects(boundsA, boundsB);

    // }

}
