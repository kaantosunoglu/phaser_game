import Phaser from 'phaser';
import CharacterGroup from '../assets/CharacterGroup';
import Cursor from '../sprites/Cursor';
import Helpers from '../globals/Helpers';
import WaveAnimation from '../assets/CursorWaveAnimation';
import GlobalBackground from '../sprites/GlobalBackground';
import Planet from '../sprites/Planet';
import StaticCursor from '../sprites/StaticCursor';
import NoMovementPopup from '../assets/NoMovement';

export default class extends Phaser.State {
    init() {
        var hitTime = 0;
        this.helper = new Helpers();
        this.onRollOveredCharacter;
        this.selectedCharacterWithKeyboard = -1;

        this.noMovement = 0;
        this.isNoMovementPopupOpen = false;
    }
    create() {
        this.leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        this.rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        this.background = new GlobalBackground({
            game: this.game,
            x: 0,
            y: -50,
            asset: 'characterselect_background'
        });
        this.game.add.existing(this.background);

        this.setPlanets()
        this.setTexts();
        this.setCharacters();
        this.setFooter();

        this.cursor = new Cursor({
            game: this.game,
            x: this.world.centerX,
            y: this.world.centerY,
            asset: 'hold'
        });

        this.game.add.existing(this.cursor);
    }

    update() {

        if (!this.isNoMovementPopupOpen) {

            if (this.game.globals.handCount > 0) {
                    this.noMovement = 0;
                
            } else if (!this.game.globals.isKeyboardActive && this.game.globals.handCount <= 0) {
                this.noMovement++;
                if (this.noMovement >= 1800) {
                    this.noMovementPopup = new NoMovementPopup({ game: this.game });
                    this.game.add.existing(this.noMovementPopup);
                    this.isNoMovementPopupOpen = true;
                }
            }

            if (!this.isKeyboardSelected) {
                if (this.hitTime === 1) {
                    this.cursor.startAnimation();
                } else if (this.hitTime === 0) {
                    this.cursor.cancelAnimation();
                }
                let anyRollOver = false;
                for (let i = 0; i < this.characters.children.length; i++) {
                    if (this.helper.checkOverlap(this.characters.children[i], this.cursor)) {
                        anyRollOver = true;
                        if (this.onRollOveredCharacter === i) {
                            this.hitTime++;
                        } else {
                            this.hitTime = 0;
                            this.onRollOveredCharacter = i;
                            for (let j = 0; j < this.characters.children.length; j++) {
                                this.characters.children[j].onRollOut();
                            }
                            this.characters.children[i].onRollOver();
                        }
                        if (this.hitTime === 155) {
                            this.state.start("Mission");
                            this.game.globals.character = i;
                        }
                    }
                }
                if (!anyRollOver) {
                    this.hitTime = 0;
                    for (let j = 0; j < this.characters.children.length; j++) {
                        this.characters.children[j].onRollOut();
                    }
                }
            }

            if (this.spaceKey.downDuration(1)) {
                if (this.selectedCharacterWithKeyboard > -1) {
                    this.game.globals.character = this.selectedCharacterWithKeyboard;
                    this.state.start("Mission");
                }
            }
            if (this.leftKey.downDuration(1)) {
                this.isKeyboardSelected = true;
                this.selectedCharacterWithKeyboard = this.selectedCharacterWithKeyboard <= 0 ? 4 : this.selectedCharacterWithKeyboard - 1;
                for (var i = 0; i < this.characters.children.length; i++) {
                    this.characters.children[i].onRollOut();
                }
                this.characters.children[this.selectedCharacterWithKeyboard].onRollOver();
            }
            if (this.rightKey.downDuration(1)) {
                this.isKeyboardSelected = true;
                this.selectedCharacterWithKeyboard = this.selectedCharacterWithKeyboard == 4 ? 0 : this.selectedCharacterWithKeyboard + 1;
                for (var i = 0; i < this.characters.children.length; i++) {
                    this.characters.children[i].onRollOut();
                }
                this.characters.children[this.selectedCharacterWithKeyboard].onRollOver();
            }
        } else {
            if (this.noMovementPopup.quit) {
                this.noMovement = 0;
                //this.noMovementPopup.destroy();
                this.state.start('Home');
                this.isNoMovementPopupOpen = false;
            } else if (this.noMovementPopup.resume) {
                this.noMovement = 0;
                this.noMovementPopup.destroy();
                this.isNoMovementPopupOpen = false;
            }
        }
    }

    setPlanets() {
        this.planetA = new Planet({
            game: this.game,
            x: -20,
            y: 90,
            asset: 'planetA',
            frameName: 'Planet_A_'
        });
        this.planetB = new Planet({
            game: this.game,
            x: 360,
            y: 10,
            asset: 'planetB',
            frameName: 'Planet_B_'
        });
        this.planetD = new Planet({
            game: this.game,
            x: 1720,
            y: 30,
            asset: 'planetD',
            frameName: 'Planet_D_'
        });

        this.planetA.width *= .8;
        this.planetA.height *= .8;
        this.planetB.width *= .28;
        this.planetB.height *= .28;
        this.planetD.width *= .85;
        this.planetD.height *= .85;

        this.game.add.existing(this.planetA);
        this.game.add.existing(this.planetB);
        this.game.add.existing(this.planetD);
    }

    setTexts() {
        this.headerText = this.add.text(0, 160, 'CHOOSE YOUR CHARACTER', {
            font: '80px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        this.headerText.x = (this.game.width - this.headerText.width) / 2;
        this.headerText.setShadow(-10, 10, 'rgba(0,0,0,1)', 0);
    }

    setFooter() {
        let footerText = this.add.text(0, this.game.height - 60, 'Move the cursor to the character you wish to choose and hold to select', {
            font: '34px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        footerText.x = (this.game.width - footerText.width) / 2;
        let holdAnim = new StaticCursor({ game: this.game });
        holdAnim.width /= 1.6;
        holdAnim.height /= 1.6;
        this.game.add.existing(holdAnim);
        holdAnim.x = (this.game.width / 2);
        holdAnim.y = this.game.height - 160;
        holdAnim.loopAnimation();
    }

    setCharacters() {
        this.characters = this.game.add.group();

        for (let i = 0; i < this.game.globals.characterNames.length; i++) {
            let character = new CharacterGroup({ game: this.game, CharacterName: this.game.globals.characterNames[i] });
            this.characters.add(character);

            character.x = (i == 0) ? 0 : this.characters.children[i - 1].x + (this.characters.children[i - 1].width / 1.2);
        }
        for (let j = 0; j < this.game.globals.characterNames.length; j++) {
            this.characters.children[j].y = this.characters.height - this.characters.children[j].height;
        }

        this.characters.x = ((this.game.width - this.characters.width) / 2) + 200;
        this.characters.y = this.game.height - 850;
    }
}
