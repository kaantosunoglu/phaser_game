import Phaser from 'phaser';
import Cursor from '../sprites/Cursor';
import WaveAnimation from '../assets/CursorWaveAnimation';
import Lostlife from '../assets/LostLife';
import Title from '../sprites/Title';
import Ship from '../sprites/ShipWithTheCharacters'
import ShipWithTheCharacters from '../sprites/ShipWithTheCharacters';
import Planet from '../sprites/Planet';
import ShipTrace from '../sprites/ShipTrace';
import Logo from '../sprites/Logo';
import GlobalBackground from '../sprites/GlobalBackground';
import Alien from '../sprites/Alien';
import NoMovementPopup from '../assets/NoMovement';

export default class extends Phaser.State {
    init() {
        var lastHandPositionX;
        this.waveCount = 0;
        this.isShipGoing = false;
        this.noMovement = 0;
        this.isNoMovementPopupOpen = false;
    }
    preload() { }

    create() {
        this.background = new GlobalBackground({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'globalbackground'
        });

        this.setTexts();
        this.setPlanets();
        this.setLogos();
        this.setShip();
        this.setAliens();

        this.titleImage = new Title({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'titleanimation'
        });
        this.titleImage.x = 380;
        this.titleImage.y = 52;
        this.game.add.existing(this.titleImage);

        this.cursor = new Cursor({
            game: this.game,
            x: this.world.centerX,
            y: this.world.centerY,
            asset: 'hold'
        });
        this.game.add.existing(this.cursor);

        this.setFooter();

        this.game.input.onDown.add(function () {

            if (game.scale.isFullScreen) {
                // turn it off
                game.scale.stopFullScreen();
            } else {
                game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
                game.scale.startFullScreen(false);
            }

        });
    }

    update() {
            if (this.game.globals.handCount > 0 && !this.isShipGoing) {   
                if (this.game.globals.swipeDirection == "wave") {
                    this.waveCount++;
                    if (this.waveCount === 2) {
                        this.isShipGoing = true;
                        this.tweenShip();
                    }
                }
            } 
            if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.tweenShip();
                this.game.globals.isKeyboardActive = true;
            }
    }


    tweenShip() {
        this.shipTween.stop();
        this.shipTraceTween.stop();
        this.shipTraceTween = game.add.tween(this.shipTrace).to({ y: 450, x: 2500 }, 2000, "Back.easeInOut", true, 0, 0, false);
        this.shipTween = game.add.tween(this.ship).to({ y: 200, x: 3500 }, 2000, "Back.easeInOut", true, 0, 0, false);
        this.shipTween.onComplete.add(this.changeState, this);
    }

    changeState() {
        this.waveCount = 0;
        this.isShipGoing = false;
        this.noMovement = 0;
        this.isNoMovementPopupOpen = false;
        this.state.start('Welcome');
    }

    setShip() {
        this.shipTrace = new ShipTrace({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'shiptrace'
        })
        this.shipTrace.x = 0;
        this.shipTrace.y = 481;
        this.game.add.existing(this.shipTrace);

        this.ship = new ShipWithTheCharacters({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'shipwiththecharacters'
        });
        this.ship.x = 825;
        this.ship.y = 168;
        var shipTargetX = this.ship.x - 30;
        var shipTargetY = this.ship.y + 30;
        this.game.add.existing(this.ship);
        this.shipTween = game.add.tween(this.ship).to({ y: shipTargetY, x: shipTargetX }, 2000, "Quad.easeInOut", true, 0, -1, true);

        var traceTargetX = this.shipTrace.x - 30;
        var traceTargetY = this.shipTrace.y + 30;
        this.shipTraceTween = game.add.tween(this.shipTrace).to({ y: traceTargetY, x: traceTargetX, alpha: .5 }, 2000, "Quad.easeInOut", true, 0, -1, true);
    }

    setTexts() {
        this.game.add.existing(this.background);
        let highScoreText = this.add.text(68, 59, 'High Score', {
            font: '50px Slackey',
            fill: '#ffa201',
            align: 'left',
            smoothed: false
        });
        highScoreText.setShadow(-3, 3, 'rgba(0,0,0,1)', 0);
        let highScore = this.add.text(70, 120, this.game.globals.highScore, {
            font: '80px Slackey',
            fill: '#fff1da',
            align: 'left',
            smoothed: false
        });
        highScore.setShadow(-3, 3, 'rgba(0,0,0,1)', 0);
    }

    setPlanets() {
        this.planetA = new Planet({
            game: this.game,
            x: 1730,
            y: 541,
            asset: 'planetA',
            frameName: 'Planet_A_'
        });
        this.planetB = new Planet({
            game: this.game,
            x: 1700,
            y: 130,
            asset: 'planetB',
            frameName: 'Planet_B_'
        });
        this.planetC = new Planet({
            game: this.game,
            x: 1330,
            y: -140,
            asset: 'planetC',
            frameName: 'Planet_C_'
        });
        this.planetD = new Planet({
            game: this.game,
            x: -320,
            y: 200,
            asset: 'planetD',
            frameName: 'Planet_D_'
        });

        this.planetC.width *= 1.3;
        this.planetC.height *= 1.3;

        this.planetB.width /= 4;
        this.planetB.height /= 4;

        this.planetD.width *= 1.7;
        this.planetD.height *= 1.7;

        this.game.add.existing(this.planetA);
        this.game.add.existing(this.planetB);
        this.game.add.existing(this.planetC);
        this.game.add.existing(this.planetD);
    }

    setLogos() {
        this.sobiLogo = new Logo({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'sobilogo'
        });
        this.sobiLogo.x = 40;
        this.sobiLogo.y = this.game.height - this.sobiLogo.height - 33;
        this.game.add.existing(this.sobiLogo);

        this.bioverativLogo = new Logo({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'bioverativlogo'
        });
        this.bioverativLogo.x = this.game.width - this.bioverativLogo.width - 40;
        this.bioverativLogo.y = this.game.height - this.bioverativLogo.height - 33;
        this.game.add.existing(this.bioverativLogo);
    }

    setAliens() {
        let alien1 = new Alien({
            game: this.game,
            x: 1578,
            y: 374,
            asset: 'alien1',
            physic: false
        });
        this.game.add.existing(alien1);

        let alien2 = new Alien({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'alien2',
            physic: false
        });
        alien2.width *= 2;
        alien2.height *= 2;
        alien2.x = 155 + (alien2.width / 2);
        alien2.y = 632 + (alien2.height / 2);
        this.game.add.existing(alien2);

        let alien3 = new Alien({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'alien3',
            physic: false
        });
        alien3.width *= 2;
        alien3.height *= 2;
        alien3.x = 582 + (alien3.width / 2);
        alien3.y = 50 + (alien3.height / 2);
        this.game.add.existing(alien3);
    }

    setFooter() {
        let footerText = this.add.text(0, 0, 'Wave your hand to start', {
            font: '34px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        footerText.x = (this.game.width - footerText.width) / 2;
        footerText.y = this.game.height - footerText.height - 30;
        this.waveAnim = new WaveAnimation({ game: this.game });
        this.game.add.existing(this.waveAnim);
        this.waveAnim.x = this.world.centerX;
        this.waveAnim.y = this.game.height - 50;
    }
}