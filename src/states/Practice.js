import Phaser from 'phaser';
import Cursor from '../sprites/Cursor';
import Ship from '../sprites/SpaceShip';
import HoldLight from '../sprites/HoldLight';
import Helpers from '../globals/Helpers';
import Alien from '../sprites/Alien';
import RightLeftAnimation from '../assets/CursorRightLeftAnimation';
import HoldAnimation from '../assets/CursorHoldAnimation';
import GlobalBackground from '../sprites/GlobalBackground';
import Planet from '../sprites/Planet';
import TrialPath from '../sprites/TrialPath';
import { rotate } from 'gl-matrix/src/gl-matrix/mat2';
import ShipWithTheCharacters from '../sprites/ShipWithTheCharacters'
import ShipTrace from '../sprites/ShipTrace';
import Blueloading from '../sprites/BlueLoading';

import NoMovementPopup from '../assets/NoMovement'

export default class extends Phaser.State {
    init() {
        this.helper = new Helpers();
        var hitTime = 0;
        this.practiceStep = 1;
        this.lastPosition = 0;
        this.killedAliens = 0;
        this.alienCount = 5;
        this.countdownTime = 2;

        this.noMovement = 0;
        this.isNoMovementPopupOpen = false;
    }

    create() {
        this.background = new GlobalBackground({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'globalbackground'
        });
        this.game.add.existing(this.background);
        this.setPlanets()
        this.setTexts();

        this.trialPath = new TrialPath({
            game: this.game,
            x: 0,
            y: 655,
            asset: 'trialpath'
        });
        this.trialPath.x = this.game.width / 2;
        this.game.add.existing(this.trialPath);

        this.trialPath2 = new TrialPath({
            game: this.game,
            x: 0,
            y: 755,
            asset: 'trialpath2'
        });
        this.trialPath2.x = this.game.width / 2;
        this.game.add.existing(this.trialPath2);
        this.trialPath2.visible = false;

        this.holdLight = new HoldLight({
            game: this.game,
            x: this.world.centerX + 490,
            y: this.world.centerY + 115,
            asset: 'grey'
        });
        this.holdLight.width = 50;
        this.holdLight.height = 50;
        this.game.add.existing(this.holdLight);
        this.holdLight.alpha = 0;


        this.ship = new Ship({
            game: this.game,
            x: 0,
            y: 650,
            asset: 'ship'
        });
        this.ship.angle = 0;
        this.ship.width *= 1.5;
        this.ship.height *= 1.5;
        this.game.add.existing(this.ship);
        this.shipCollisionGroup = this.physics.p2.createCollisionGroup();
        this.ship.body.setCollisionGroup(this.shipCollisionGroup);

        // "aliens" is group for aliens.
        // We won't try to dedect to if alien hit ship. We'll try dedect if group element hit the ship
        this.aliens = this.game.add.physicsGroup(Phaser.Physics.P2JS);
        this.alienCollisionGroup = this.physics.p2.createCollisionGroup();
        //this.ship.body.collides(this.alienCollisionGroup);
        this.ship.body.collides(this.alienCollisionGroup, this.alienHitShip, this);

        this.setShip();
        this.cursor = new Cursor({
            game: this.game,
            x: this.world.centerX,
            y: this.world.centerY,
            asset: 'hold'
        });
        this.cursor.visible = false;
        this.game.add.existing(this.cursor);

        this.graphics = this.game.add.graphics(0, 0);
        this.graphics.lineStyle(0);
        this.graphics.beginFill(0x000000, 0.8);
        this.graphics.drawRect(0, 0, this.game.width, this.game.height);
        this.graphics.endFill();
        this.graphics.visible = false;

        this.niceWorkText = this.add.text(0, 180, `NICE WORK!`, {
            font: '120px Slackey',
            fill: '#53c7f1',
            align: 'center',
            smoothed: false,
            boundsAlignH: "center",
            boundsAlignV: "bottom"
        });
        this.niceWorkText.x = (this.game.width - this.niceWorkText.width) / 2;
        this.niceWorkText.y = ((this.game.height - this.niceWorkText.height) / 2);
        this.niceWorkText.padding.y = 33;
        this.niceWorkText.setShadow(-20, 20, 'rgba(0,0,0,1)', 0);
        this.niceWorkText.visible = false;

        this.setFooter();

        this.leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        this.rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        this.setScene();

    }

    update() {

        if (!this.isNoMovementPopupOpen) {

            if (this.game.globals.handCount > 0) {
                this.noMovement = 0;
            } else if (!this.game.globals.isKeyboardActive && this.game.globals.handCount <= 0) {
                this.noMovement++;
                if (this.noMovement >= 1800) {
                    this.game.physics.p2.pause();
                    this.game.time.events.pause();
                    this.noMovementPopup = new NoMovementPopup({ game: this.game });
                    this.game.add.existing(this.noMovementPopup);
                    this.isNoMovementPopupOpen = true;
                }
            }

            if (this.practiceStep === 2 || this.practiceStep === 4 || this.practiceStep === 7 || this.practiceStep === 8) {
                if (this.spaceKey.downDuration(1)) {
                    this.changePracticeStep();
                }
                if (this.game.globals.handCount > 0 && this.practiceStep === 8) {
                    if (this.game.globals.handX > 750) {
                        this.isHandAtTheRight = true;
                        this.readyToSwipe = true;
                    }
                    if (this.readyToSwipe && this.game.globals.handX < 600 && this.isHandAtTheRight) {
                        this.isHandAtTheRight = false;
                        this.changePracticeStep();
                    }

                } else {
                    this.isHandAtTheRight = false;
                    this.cursor.cancelAnimation();
                    this.readyToSwipe = false;
                    this.isHolded = false;
                    this.holding = 0;
                }
            } else {
                if (this.practiceStep === 5 || this.practiceStep === 6) {
                    this.ship.body.x < (this.trialPath2.x - (this.trialPath2.width / 2)) ? this.ship.body.x = this.trialPath2.x - (this.trialPath2.width / 2) : this.ship.body.x;
                    this.ship.body.x > this.trialPath2.x + (this.trialPath2.width / 2) ? this.ship.body.x = this.trialPath2.x + (this.trialPath2.width / 2) : this.ship.body.x;

                }
                if (!this.isKeyboardSelected) {
                    this.ship.body.x = this.game.globals.handX;
                    if (this.practiceStep === 1 || this.practiceStep === 3) {
                        this.ship.body.x < this.world.centerX - 500 ? this.ship.body.x = this.world.centerX - 500 : this.ship.body.x;
                        this.ship.body.x > this.world.centerX + 500 ? this.ship.body.x = this.world.centerX + 500 : this.ship.body.x;
                        if (this.practiceStep === 1 && this.ship.body.x >= this.holdLight.x) this.changePracticeStep();
                        if (this.practiceStep === 3 && this.ship.body.x <= this.holdLight.x) this.changePracticeStep();
                    } else if (this.practiceStep === 5 || this.practiceStep === 6) {
                        this.ship.body.x < (this.trialPath2.x - (this.trialPath2.width / 2)) ? this.ship.body.x = this.trialPath2.x - (this.trialPath2.width / 2) : this.ship.body.x;
                        this.ship.body.x > this.trialPath2.x + (this.trialPath2.width / 2) ? this.ship.body.x = this.trialPath2.x + (this.trialPath2.width / 2) : this.ship.body.x;
                        if (this.practiceStep === 5 && this.ship.body.x >= (this.trialPath2.x + (this.trialPath2.width / 2)) - 100) {

                            this.changePracticeStep();
                        }
                    }
                } else {
                    this.ship.body.setZeroVelocity();
                }

                if (this.leftKey.isDown) {
                    this.game.globals.isKeyboardActive = true;
                    this.ship.body.moveLeft(800);
                    if (this.practiceStep === 1 || this.practiceStep === 3) {
                        this.ship.body.x < this.world.centerX - 500 ? this.ship.body.x = this.world.centerX - 500 : this.ship.body.x;
                        this.ship.body.x > this.world.centerX + 500 ? this.ship.body.x = this.world.centerX + 500 : this.ship.body.x;
                        if (this.ship.body.x === this.holdLight.x) {
                            this.changePracticeStep();
                        }
                    } else if (this.practiceStep === 5 || this.practiceStep === 6) {
                        this.ship.body.x < (this.trialPath2.x - (this.trialPath2.width / 2)) ? this.ship.body.x = this.trialPath2.x - (this.trialPath2.width / 2) : this.ship.body.x;
                        this.ship.body.x > this.trialPath2.x + (this.trialPath2.width / 2) ? this.ship.body.x = this.trialPath2.x + (this.trialPath2.width / 2) : this.ship.body.x;
                        if (this.practiceStep === 5 && this.ship.body.x >= (this.trialPath2.x + (this.trialPath2.width / 2))) {
                            this.changePracticeStep();
                        }
                    }
                    this.isKeyboardSelected = true;
                }
                else if (this.rightKey.isDown) {
                    this.game.globals.isKeyboardActive = true;
                    this.ship.body.moveRight(800);
                    if (this.practiceStep === 1 || this.practiceStep === 3) {
                        this.ship.body.x < this.world.centerX - 500 ? this.ship.body.x = this.world.centerX - 500 : this.ship.body.x;
                        this.ship.body.x > this.world.centerX + 500 ? this.ship.body.x = this.world.centerX + 500 : this.ship.body.x;

                        if (this.ship.body.x >= this.holdLight.x) {
                            this.changePracticeStep();
                        }
                    } else if (this.practiceStep === 5 || this.practiceStep === 6) {
                        this.ship.body.x < (this.trialPath2.x - (this.trialPath2.width / 2)) ? this.ship.body.x = this.trialPath2.x - (this.trialPath2.width / 2) : this.ship.body.x;
                        this.ship.body.x > this.trialPath2.x + (this.trialPath2.width / 2) ? this.ship.body.x = this.trialPath2.x + (this.trialPath2.width / 2) : this.ship.body.x;
                        if (this.practiceStep === 5 && this.ship.body.x >= (this.trialPath2.x + (this.trialPath2.width / 2))) {

                            this.changePracticeStep();
                        }
                    }
                    this.isKeyboardSelected = true;
                }

                this.helper.setShipAngle(this.ship.body.x, this.lastPosition, this.ship);
                this.lastPosition = this.ship.body.x;

            }
        } else {
            if (this.noMovementPopup.quit) {
                this.game.physics.p2.resume();
                this.game.time.events.resume();
                this.state.start('Home');
                 this.noMovement = 0;
                 //this.noMovementPopup.destroy();
                 this.isNoMovementPopupOpen = false;
            } else if (this.noMovementPopup.resume) {
                this.game.physics.p2.resume();
                this.game.time.events.resume();
                this.noMovement = 0;
                this.noMovementPopup.destroy();
                this.isNoMovementPopupOpen = false;
            }
        }
    }

    setPlanets() {
        this.planetA = new Planet({
            game: this.game,
            x: 1330,
            y: 250,
            asset: 'planetA',
            frameName: 'Planet_A_'
        });
        this.planetB = new Planet({
            game: this.game,
            x: -70,
            y: -190,
            asset: 'planetB',
            frameName: 'Planet_B_'
        });
        this.planetC = new Planet({
            game: this.game,
            x: -250,
            y: 700,
            asset: 'planetC',
            frameName: 'Planet_C_'
        });
        this.planetD = new Planet({
            game: this.game,
            x: 1580,
            y: 0,
            asset: 'planetD',
            frameName: 'Planet_D_'
        });

        this.planetA.width *= 2.7;
        this.planetA.height *= 2.7;
        this.planetA.angle = -55;

        this.planetB.width *= 1.4;
        this.planetB.height *= 1.4;

        this.planetC.width *= 1.3;
        this.planetC.height *= 1.3;

        this.planetD.width *= 1.6;
        this.planetD.height *= 1.6;

        //this.game.add.existing(this.planetA);
        this.game.add.existing(this.planetB);
        this.game.add.existing(this.planetC);
        this.game.add.existing(this.planetD);
    }

    setTexts() {
        this.headerText = this.add.text(0, 160, 'LET\'S TRY IT', {
            font: '80px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        this.headerText.x = (this.game.width - this.headerText.width) / 2;
        this.headerText.setShadow(-10, 10, 'rgba(0,0,0,1)', 0);
        // change name
        this.infoText = this.add.text(0, 320, 'First move your hand to the right\nto navigate your spaceship towards the circle', {
            font: '42px Open Sans',
            fill: '#ffffff',
            align: 'center',
            smoothed: false
        });
        this.infoText.x = (this.game.width - this.infoText.width) / 2;
    }


    setFooter() {
        this.footerText = this.add.text(0, this.game.height - 60, 'Swipe your hand from right to left to continue', {
            font: '34px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        this.footerText.x = (this.game.width - this.footerText.width) / 2;
        this.footerText.y = this.game.height - this.footerText.height - 30;

        this.moveAnim = new RightLeftAnimation({ game: this.game });
        this.moveAnim.height /= 1.6;
        this.moveAnim.width /= 1.6;
        this.game.add.existing(this.moveAnim);
        this.moveAnim.x = this.game.width / 2;
        this.moveAnim.y = this.game.height - 120;

        this.holdAnim = new HoldAnimation({ game: this.game });
        this.holdAnim.height /= 1.6;
        this.holdAnim.width /= 1.6;
        this.game.add.existing(this.holdAnim);
        this.holdAnim.x = this.game.width / 2;
        this.holdAnim.y = this.game.height - 120;



    }

    changePracticeStep() {
        if (this.changeStateTween != null) this.changeStateTween.stop();
        this.ship.body.moveLeft(0);
        this.practiceStep++;
        //console.log(this.practiceStep);
        this.holding = 0;
        this.setScene();
    }

    setScene() {

        // practiceSteps
        //
        // 1 - Move ship to right
        // 2 - Nice Work screen and hold hand
        // 3 - Move ship to left
        // 4 - Nice Work Screen and hold hand
        // 5 - Hold hand and move ship from left to right to begin
        // 6 - Practice with aliens
        // 7 - Nice work screen and hold hand


        if (this.practiceStep === 1) {
            this.moveAnim.visible = true;
            this.holdAnim.visible = false;

        } else if (this.practiceStep === 2 || this.practiceStep === 4) {
            this.moveAnim.visible = false;
            this.holdAnim.visible = true;
            this.graphics.visible = true;
            this.niceWorkText.visible = true;
            this.footerText.setText("Hold your hand");
            this.footerText.x = (this.game.width - this.footerText.width) / 2;
            this.footerText.visible = false;
            this.countdownTime = 2;
            this.countdownForNiceWork();
            //this.changeStateTween = game.add.tween(this).to({}, 3000, "Back.easeInOut", true, 0, 0, false);
            //this.changeStateTween.onComplete.add(this.changePracticeStep, this);
        } else if (this.practiceStep === 3) {
            this.moveAnim.visible = true;
            this.holdAnim.visible = false;
            this.graphics.visible = false;
            this.niceWorkText.visible = false;
            this.footerText.visible = true;
            this.holdLight.x = this.world.centerX - 500;
            this.trialPath.rotateImage();
            this.infoText.setText("First move your hand to the left\nto navigate your spaceship towards the circle");
            this.footerText.setText("Move your hand to the left");
            this.infoText.x = (this.game.width - this.infoText.width) / 2;
            this.footerText.x = (this.game.width - this.footerText.width) / 2;
        } else if (this.practiceStep === 5) {
            this.moveAnim.visible = true;
            this.holdAnim.visible = false;
            this.ship.angle = 0;
            this.graphics.visible = false;
            this.niceWorkText.visible = false;
            this.footerText.visible = true;
            this.ship.body.y = this.world.centerY + 200;
            this.ship.body.x = this.graphics.x;
            this.holdLight.visible = false;
            this.trialPath.visible = false;
            this.trialPath2.visible = true;
            this.infoText.setText("Finally, move your hand to defend your planet’s\nprotection zone from 5 incoming aliens.\nHitting an alien with your spaceship\nwill cause it to disintegrate.");
            this.footerText.setText("Swipe your hand from right to left to begin");
            this.infoText.x = (this.game.width - this.infoText.width) / 2;
            this.footerText.x = (this.game.width - this.footerText.width) / 2;
        } else if (this.practiceStep === 6) {
            this.moveAnim.visible = true;
            this.holdAnim.visible = false;
            this.headerText.visible = false;
            this.infoText.visible = false;
            this.footerText.setText("Move your hand left and right");
            this.footerText.x = (this.game.width - this.footerText.width) / 2;
            //this.ship.body.collides(this.alienCollisionGroup);

            this.setUpAliens();
        } else if (this.practiceStep === 7) {
            this.moveAnim.visible = false;
            this.holdAnim.visible = true;
            this.footerText.setText("Hold your hand");
            this.footerText.x = (this.game.width - this.footerText.width) / 2; this.niceWorkText.kill();
            this.footerText.visible = false;
            this.aliens.killAll();
            game.time.events.remove(this.timeEvent);
            this.graphics.visible = true;
            this.niceWorkText.visible = true;
            this.countdownTime = 2;
            this.countdownForNiceWork();
            //this.changeStateTween = game.add.tween(this).to({}, 3000, "Back.easeInOut", true, 0, 0, false);
            //this.changeStateTween.onComplete.add(this.changePracticeStep, this);
        } else if (this.practiceStep === 8) {
            this.moveAnim.visible = true;
            this.holdAnim.visible = false;
            this.ship2.visible = true;
            this.shipTrace.visible = true;
            this.headerText.visible = true;
            this.infoText.visible = true;
            this.trialPath2.visible = false;
            this.cursor.cancelAnimation();
            this.readyToSwipe = false;
            this.isHolded = false;
            this.holding = 0;
            this.cursor.visible = true;
            this.headerText.setText("LET'S GO!");
            this.headerText.x = (this.game.width - this.headerText.width) / 2;
            this.infoText.setText("We think you’re ready to defend your planet’s\nprotection zone from the alien invasion!");
            this.footerText.setText("Swipe your hand from right to left to continue");
            this.footerText.x = (this.game.width - this.footerText.width) / 2;
            this.niceWorkText.kill();
            this.footerText.visible = true;
            this.graphics.kill();
            this.ship.kill();

        } else if (this.practiceStep === 9) {
            // this.state.start('AvatarSelect');
            this.tweenShip();
        }
    }

    countdownForNiceWork() {
        if (this.countdownTime > 0) {
            this.countdownTime--;
            this.changeStateTween = game.add.tween(this).to({}, 250, "Back.easeInOut", true, 0, 0, false);
            this.changeStateTween.onComplete.add(this.countdownForNiceWork, this);
        } else {
            this.changeStateTween = game.add.tween(this).to({}, 250, "Back.easeInOut", true, 0, 0, false);
            this.changeStateTween.onComplete.add(this.changePracticeStep, this);
        }

    }

    tweenShip() {
        this.shipTween.stop();
        this.shipTraceTween.stop();
        this.shipTraceTween = game.add.tween(this.shipTrace).to({ y: 450, x: 2500 }, 2000, "Back.easeInOut", true, 0, 0, false);
        this.shipTween = game.add.tween(this.ship2).to({ y: 200, x: 3500 }, 2000, "Back.easeInOut", true, 0, 0, false);
        this.shipTween.onComplete.add(this.changeState, this);
    }

    changeState() {
        this.state.start('AvatarSelect');
    }

    setShip() {
        this.shipTrace = new ShipTrace({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'shiptrace'
        })
        this.shipTrace.x = 175;
        this.shipTrace.y = 721;
        this.game.add.existing(this.shipTrace);

        this.ship2 = new ShipWithTheCharacters({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'shipwiththecharacters'
        });
        this.ship2.x = 1000;
        this.ship2.y = 408;
        var shipTargetX = this.ship2.x - 30;
        var shipTargetY = this.ship2.y + 30;
        this.game.add.existing(this.ship2);
        this.shipTween = game.add.tween(this.ship2).to({ y: shipTargetY, x: shipTargetX }, 2000, "Quad.easeInOut", true, 0, -1, true);

        var traceTargetX = this.shipTrace.x - 30;
        var traceTargetY = this.shipTrace.y + 30;
        this.shipTraceTween = game.add.tween(this.shipTrace).to({ y: traceTargetY, x: traceTargetX, alpha: .5 }, 2000, "Quad.easeInOut", true, 0, -1, true);
        this.ship2.visible = false;
        this.shipTrace.visible = false;
    }

    alienHitShip(ship, alien) {
        //console.log("HIT 1");

        //move alien
        if (!alien.sprite.isDestroyed) {
            alien.kinematic = true;
            alien.velocity.x = alien.velocity.y = 0;
            //console.log("HIT 2");
            alien.sprite.destroyAnimation();
            this.killedAliens++;
            //if (this.killedAliens === 5) this.changePracticeStep();
        }
    }

    setUpAliens() {
        if (this.alienCount > 0) {
            this.alienCount--;
            // call summonAlien function in random time later
            this.timeEvent = this.game.time.events.add(Phaser.Timer.SECOND * 2, this.summonAlien, this);
        } else {
            this.timeEvent = this.game.time.events.add(Phaser.Timer.SECOND * 5, this.changePracticeStep, this);
            // this.changePracticeStep();
        }
    }

    summonAlien() {
        let alienMode = Math.floor(Math.random() * 10) + 1;
        if (alienMode === 10 || alienMode === 9) {
            let alienX = (Math.random() * (this.game.width - 200)) + 100;
            this.alien = new Alien({
                game: this.game,
                x: alienX,
                y: -10,
                asset: 'alien1',
                point: 5,
                physic: true
            });
            this.aliens.add(this.alien);
        } else if (alienMode <= 8 && alienMode >= 6) {
            let alienX = (Math.random() * (this.game.width - 200)) + 100;
            this.alien = new Alien({
                game: this.game,
                x: alienX,
                y: -10,
                asset: 'alien2',
                point: 3,
                physic: true
            });
            this.aliens.add(this.alien);
        } else if (alienMode < 6) {
            let alienX = (Math.random() * (this.game.width - 200)) + 100;
            this.alien = new Alien({
                game: this.game,
                x: alienX,
                y: -10,
                asset: 'alien3',
                point: 2,
                physic: true
            });
            // add alien to screen
            this.aliens.add(this.alien);
            //call again setUpAliens
        }
        this.alien.body.setCollisionGroup(this.alienCollisionGroup);
        this.alien.body.collides(this.shipCollisionGroup);
        this.alien.body.velocity.y = 300;
        this.setUpAliens();
    }

}
