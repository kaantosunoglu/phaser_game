import Phaser from 'phaser';
import Cursor from '../sprites/Cursor';
import ChooseButton from '../assets/ChooseButton';
import Helper from '../globals/Helpers';
import StatichandCursor from '../sprites/StaticCursor';

export default class extends Phaser.Group {
    constructor({ game }) {
        super(game);
        this.game = game;
        this.helper = new Helper();
        this.hitTime = 0;
        this.quit = false;
        this.resume = false;

        this.setBackgrounds();
        this.setTexts();
        this.setButtons();
        this.setFooter();

        this.handcursor = new Cursor({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'hold'
        });
        this.game.add.existing(this.handcursor);
        this.addTimer();
    }



    update() {
        if (this.game.globals.handCount > 0) {
            if (this.helper.checkOverlap(this.retryButton, this.handcursor)) {
                if (this.hitTime == 1) {
                    this.retryButton.startAnimation();
                }
                this.hitTime++;
                //console.log(this.game.time.fps + " hit " + this.hitTime)
                if (this.hitTime >= ((this.game.time.fps * 155) / 60)) {
                    this.retryGame();
                }
            } else if (this.helper.checkOverlap(this.quitButton, this.handcursor)) {
                if (this.hitTime == 1) {
                    this.quitButton.startAnimation();
                }
                this.hitTime++;
                //console.log(this.game.time.fps + " hit " + this.hitTime)
                if (this.hitTime >= ((this.game.time.fps * 155) / 60)) {
                    this.quitGame();
                }
            }
            else {
                this.retryButton.stopAnimation();
                this.quitButton.stopAnimation();
                this.hitTime = 0;
            }
        } else {
            this.retryButton.stopAnimation();
            this.quitButton.stopAnimation();
            this.hitTime = 0;
        }

        if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
            this.game.globals.isKeyboardActive = true;
            this.quitGame();
        }
        if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
            this.game.globals.isKeyboardActive = true;
            this.retryGame();
        }


    }

    setBackgrounds() {
        this.graphics = this.game.add.graphics(0, 0);
        this.graphics.lineStyle(0);
        this.graphics.beginFill(0x291029, 1);
        this.graphics.drawRect(0, 0, this.game.width, this.game.height);
        this.graphics.endFill();

        this.add(this.graphics);
    }

    setTexts() {
        this.headerText = new Phaser.Text(this.game, 0, 160, `NO MOVEMENT DETECTED`, {
            font: '80px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        this.headerText.x = (this.game.width - this.headerText.width) / 2;
        this.headerText.setShadow(-10, 10, 'rgba(0,0,0,0.5)', 0);
        this.add(this.headerText);

        this.infoText = new Phaser.Text(this.game, 0, 320, `Try moving your hands closer\nto the sensor or ask for assistance`, {
            font: '42px Open Sans',
            fill: '#ffffff',
            align: 'center',
            smoothed: false
        });
        this.infoText.x = (this.game.width - this.infoText.width) / 2;
        this.add(this.infoText);

        this.gameOverText = new Phaser.Text(this.game, 0, 450, `Do you wish to continue?`, {
            font: '48px Slackey',
            fill: '#53c7f1',
            align: 'center',
            smoothed: false
        });
        this.gameOverText.x = (this.game.width - this.gameOverText.width) / 2;
        this.gameOverText.setShadow(-4, 4, 'rgba(0,0,0,0.5)', 0);
        this.add(this.gameOverText);
    }

    setButtons() {
        this.quitButton = new ChooseButton({
            game: this.game,
            buttonText: 'NO'
        });
        this.quitButton.y = 645;
        this.quitButton.x = ((this.game.width - this.quitButton.width) / 2) - 400;

        this.retryButton = new ChooseButton({
            game: this.game,
            buttonText: 'YES'
        });
        this.retryButton.y = 645;
        this.retryButton.x = ((this.game.width - this.retryButton.width) / 2) + 400;

        this.add(this.quitButton);
        this.add(this.retryButton);
    }

    setFooter() {

        let footerText = new Phaser.Text(this.game, 0, this.game.height - 60, 'Hold your hand over the button you wish to choose', {
            font: '34px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        footerText.x = (this.game.width - footerText.width) / 2;
        this.add(footerText);

        this.holdAnim = new StatichandCursor({ game: this.game });
        this.holdAnim.width /= 1.6;
        this.holdAnim.height /= 1.6;
        this.add(this.holdAnim);
        this.holdAnim.x = (this.game.width / 2);
        this.holdAnim.y = this.game.height - 160;
        this.holdAnim.loopAnimation();
    }

    addTimer() {
        this.turnToHomeScreen = game.add.tween(this).to({}, 15000, "Linear", true, 0, 0, false);
        this.turnToHomeScreen.onComplete.add(this.quitGame, this);
    }

    quitGame() {
        if (this.turnToHomeScreen != null) {
            this.turnToHomeScreen.stop();
            this.turnToHomeScreen = null;
        }
        this.handcursor.visible = false;
        //console.log("QUIT")
        this.quit = true;
        this.hitTime = 0;
        this.holdAnim.kill();
    }

    retryGame() {
        if (this.turnToHomeScreen != null) {
            this.turnToHomeScreen.stop();
            this.turnToHomeScreen = null;
        }
        this.handcursor.visible = false;
        //console.log("RESUME")
        this.resume = true;
        this.hitTime = 0;
        this.holdAnim.kill();
    }
}