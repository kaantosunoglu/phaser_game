import Phaser from 'phaser';
import LevelBack from '../sprites/LevelBack'

export default class extends Phaser.Group {
    constructor({ game, level }) {
        super(game);
        this.game = game;
        this.level = level;
        this.setLevelImages(this.level);
    }

    setLevelImages({ level }) {
        //smoke

        //back mountains
        this.backmountains = new LevelBack({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'level' + this.level + '_3'
        });
        this.add(this.backmountains);
        this.backmountains.x = (this.game.width - this.backmountains.width) / 2;
        this.backmountains.y = this.height - this.backmountains.height;

        //front mountains
        this.frontmountains = new LevelBack({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'level' + this.level + '_2'
        });
        this.add(this.frontmountains);
        this.frontmountains.x = (this.game.width - this.frontmountains.width) / 2;
        this.frontmountains.y = this.height - this.frontmountains.height;

        //floor

        this.floor = new LevelBack({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'level' + this.level + '_1'
        });
        this.add(this.floor);
        this.floor.x = (this.game.width - this.floor.width) / 2;
        this.floor.y = this.height - this.floor.height;

        this.protectionZone = new LevelBack({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'zonelevel_' + this.level
        });
        this.add(this.protectionZone);
        this.protectionZone.x = (this.game.width - this.protectionZone.width) / 2;
        this.protectionZone.y = this.height - 220;

        this.leftProtectionZone = new Phaser.Text(this.game, 0, 0, 'PROTECTION ZONE', {
            font: '26px Slackey',
            fill: '#d64035',
            smoothed: false,
            align: 'left'
          });
          this.leftProtectionZone.setShadow(-3, 3, 'rgba(0,0,0,1)', 0);
          this.leftProtectionZone.x = this.protectionZone.x + 100;
          this.leftProtectionZone.y = this.protectionZone.y - 10;
          this.add(this.leftProtectionZone);

          this.rightProtectionZone = new Phaser.Text(this.game,0, 0, 'PROTECTION ZONE', {
            font: '26px Slackey',
            fill: '#d64035',
            smoothed: false,
            align: 'left'
          });
          this.rightProtectionZone.setShadow(-3, 3, 'rgba(0,0,0,1)', 0);
          this.rightProtectionZone.x = this.protectionZone.x + this.protectionZone.width - this.rightProtectionZone.width + 20;
          this.rightProtectionZone.y = this.protectionZone.y - 10;
          this.add(this.rightProtectionZone);

    }

    changeBackground( {level} ) {
         this.backmountains.loadTexture('level' + level + '_3');
        this.frontmountains.loadTexture('level' + level + '_2');
        this.floor.loadTexture('level' + level + '_1');
        this.protectionZone.loadTexture('zonelevel_' + level);
        this.leftProtectionZone.addColor(level == 2 ? '#5bc3ce' : '#ff8344', 0);
        this.rightProtectionZone.addColor(level == 2 ? '#5bc3ce' : '#ff8344', 0);
        if(level == 3) {
            this.floor.y = 258;
        }
    }
    updateBackground({ shipPositionX }) {
        let gapPosition = shipPositionX - (this.game.width / 2);
        this.floor.x = gapPosition / 16;
        this.frontmountains.x = gapPosition / 32;
        this.backmountains.x = gapPosition / 70;
    }

}
