import Phaser from 'phaser';

export default class extends Phaser.Group {
    constructor({ game }) {
        super(game);
        

        this.graphics = this.game.add.graphics(0, 0);
        this.graphics.lineStyle(0);
        this.graphics.beginFill(0x291029, 0.9);
        this.graphics.drawRect(0, 0, this.game.width, this.game.height);
        this.graphics.endFill();

        this.add(this.graphics);


        this.headerText = new Phaser.Text(this.game, 0, 280, 'OH NO!', {
            font: '80px Slackey',
            fill: '#ffa201',
            smoothed: false,
            align: 'center'
        });
        this.headerText.x = (this.width - this.headerText.width) / 2;
        this.headerText.setShadow(-10, 10, 'rgba(0,0,0,1)', 0);
        this.add(this.headerText);

        this.infoText = new Phaser.Text(this.game, 0, 400, 'An alien has entered the Protection Zone', {
            font: '42px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        this.infoText.x = (this.game.width - this.infoText.width) / 2;
        this.add(this.infoText);

        this.lostText = new Phaser.Text(this.game, 0, 565, 'YOU’VE LOST A TURN!', {
            font: '100px Slackey',
            fill: '#53c7f1',
            smoothed: false,
            align: 'center'
        });
        this.lostText.x = (this.width - this.lostText.width) / 2;
        this.lostText.setShadow(-14, 14, 'rgba(0,0,0,1)', 0);
        this.add(this.lostText);

    }



}