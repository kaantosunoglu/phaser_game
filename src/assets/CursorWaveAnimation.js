import Phaser from 'phaser';

export default class extends Phaser.Group {
    constructor({ game }) {
        super(game);
        this.linesWave = this.create(0, 0, 'lineswave');
        //this.linesWave.width = 185;
        //this.linesWave.height = 49;
        this.linesWave.anchor.setTo (0.5, 2);
        this.cursor = this.create(0, 10, 'cursor');
        //this.cursor.width /= 2;
        //this.cursor.height /= 2;
        //this.cursor.width = 83;
        //this.cursor.height = 104;
        
        this.cursor.anchor.setTo (0.5, 1.5);
        this.cursor.angle = -30;
        game.add.tween(this.cursor).to({ angle: 30 }, 600, "Quad.easeInOut", true, 0, -1, true);
        
    }
}