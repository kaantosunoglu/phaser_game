import Phaser from 'phaser';
import CharacterGroup from '../sprites/Character';
import RightLeftAnimation from '../assets/CursorRightLeftAnimation';
import GlobalBackground from '../sprites/GlobalBackground';
import Planet from '../sprites/Planet';
import Blueloading from '../sprites/BlueLoading';
import ShipWithTheCharacters from '../sprites/ShipWithTheCharacters';
import ShipTrace from '../sprites/ShipTrace';

export default class extends Phaser.Group {
    constructor({ game }) {
        super(game);
        this.game = game;

        this.phase = 0;
        this.level = 0;
        this.countdownTime = 3;

        this.setBackgrounds();
        this.setPlanets({ level: 0 });
        this.setCharacters();
        this.setCounter();
        this.setTexts();

        this.setFooter();
        this.selfieScreen();
        this.shipTrace = new ShipTrace({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'shiptrace'
        })
        this.add(this.shipTrace);
        this.shipTrace.visible = false;

        this.ship = new ShipWithTheCharacters({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'shipwiththecharacters'
        });
        this.add(this.ship);
        this.ship.visible = false;

        this.readyForNext = true;
        this.changeState = false;
    }

    setCounter() {
        this.loading = new Blueloading({
            game: this.game,
            x: 0,
            y: 0,
            asset: "blueloading"
        });
        this.add(this.loading);
        this.loading.x = ((this.game.width - this.loading.width) / 2) + (this.loading.width / 2);
        this.loading.y = ((this.game.height - this.loading.height) / 2) + (this.loading.height / 2) + 10;
        this.loading.visible = false;

        this.timerText = new Phaser.Text(this.game, 0, 550, "3", {
            font: '144px Slackey',
            fill: '#53c7f1',
            smoothed: false,
            align: 'center'
        });
        this.timerText.y = (this.game.height - this.timerText.height) / 2;
        this.timerText.x = (this.game.width - this.timerText.width) / 2;
        this.add(this.timerText);
    }

    setTexts() {

        this.missionText = new Phaser.Text(this.game, 0, 280, 'READY TO START IN', {
            font: '80px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        this.missionText.x = (this.game.width - this.missionText.width) / 2;
        this.missionText.setShadow(-10, 10, 'rgba(0,0,0,1)', 0);
        this.missionText.alpha = 0;
        this.game.add.tween(this.missionText).to({ alpha: 1 }, 800, "Linear", true, 0, 0, false);
        this.missionText.visible = false;
        this.add(this.missionText);

        this.levelText = new Phaser.Text(this.game, 0, 160, 'LEVEL 1', {
            font: '60px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: false
        });
        this.levelText.y = 160;
        this.levelText.x = (this.game.width - this.levelText.width) / 2;
        this.levelText.setShadow(-10, 10, 'rgba(0,0,0,1)', 0);
        this.levelText.visible = false;
        this.levelText.visible = false;
        this.add(this.levelText);

        this.headerText = new Phaser.Text(this.game, 0, 430, "", {
            font: '80px Slackey',
            fill: '#ffa201',
            smoothed: false,
            align: 'center'
        });
        this.headerText.setShadow(-10, 10, 'rgba(0,0,0,0.5)', 0);
        this.add(this.headerText);

        this.wellDoneText = new Phaser.Text(this.game, 0, 700, "", {
            font: '42px Open Sans',
            fill: '#ffffff',
            smoothed: false,
            align: 'center',
            wordWrap: true,
            wordWrapWidth: 1005,
            smoothed: false
        });
        this.add(this.wellDoneText);
        this.levelInfoText = new Phaser.Text(this.game, 0, 220, "", {
            font: '54px Open Sans',
            fill: '#ffffff',
            smoothed: false,
            align: 'center',
            wordWrap: true,
            wordWrapWidth: 1360,
            smoothed: false
        });
        this.add(this.levelInfoText);
        this.startNewLevelText = new Phaser.Text(this.game, 0, 655, "", {
            font: '42px Slackey',
            fill: '#53c7f1',
            smoothed: false,
            align: 'center',
            wordWrap: true,
            wordWrapWidth: 1250
        });

        this.add(this.startNewLevelText);
        this.startNewLevelText.visible = false;


    }

    setCharacters() {
        this.selectedCharacter = new CharacterGroup({
            game: this.game,
            x: 380,
            y: 90,
            asset: this.game.globals.characterNames[this.game.globals.character] + "Head",
            animate: true
        });
        this.selectedCharacter.width = 300;
        this.selectedCharacter.height = 300;
        this.selectedCharacter.y = 90 + (this.selectedCharacter.height / 2);
        this.selectedCharacter.x = ((this.game.width - this.selectedCharacter.width) / 2) + (this.selectedCharacter.width / 2);
        this.add(this.selectedCharacter);
        this.selectedCharacter.visible = false;
    }

    setBackgrounds() {
        this.graphics = this.game.add.graphics(0, 0);
        this.graphics.lineStyle(0);
        this.graphics.beginFill(0x291029, 0.9);
        this.graphics.drawRect(0, 0, this.game.width, this.game.height);
        this.graphics.endFill();

        this.add(this.graphics);
        this.background = new GlobalBackground({
            game: this.game,
            x: 0,
            y: 0,
            asset: 'globalbackground'
        });
        this.add(this.background);
        this.background.visible = false;
    }

    setFooter() {
        this.footerText = new Phaser.Text(this.game, 0, this.game.height - 60, 'Swipe your hand from right to left to continue', {
            font: '34px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        this.footerText.x = (this.game.width - this.footerText.width) / 2;
        this.add(this.footerText);

        this.moveAnim = new RightLeftAnimation({ game: this.game });
        this.moveAnim.height /= 1.6;
        this.moveAnim.width /= 1.6;
        this.game.add.existing(this.moveAnim);
        this.moveAnim.x = this.game.width / 2;
        this.moveAnim.y = this.game.height - 120;
    }

    setShip({ level }) {
        this.shipTrace.visible = true;
        this.shipTrace.visible = true;
        if (level == 1) {
            this.background.visible = true;
            this.shipTrace.scale.x = -1;
            this.shipTrace.width *= .5;
            this.shipTrace.height *= .5;
            this.shipTrace.x = 2000;
            this.shipTrace.y = 750;

            this.ship.scale.x = -1;
            this.ship.width *= .5;
            this.ship.height *= .5;
            this.ship.x = 1600;
            this.ship.y = 650;
        } else if (level == 2) {
            this.background.visible = true;
            //this.shipTrace.scale.x = -1;
            this.shipTrace.width *= 1.1;
            this.shipTrace.height *= 1.1;
            this.shipTrace.x = 200;
            this.shipTrace.y = 650;

            //this.ship.scale.x = -1;
            this.ship.width *= 1.1;
            this.ship.height *= 1.1;
            this.ship.x = 1000;
            this.ship.y = 350;
        }

        var shipTargetX = this.ship.x - 30;
        var shipTargetY = this.ship.y + 30;
        this.shipTween = game.add.tween(this.ship).to({ y: shipTargetY, x: shipTargetX }, 2000, "Quad.easeInOut", true, 0, -1, true);

        var traceTargetX = this.shipTrace.x - 30;
        var traceTargetY = this.shipTrace.y + 30;
        this.shipTraceTween = game.add.tween(this.shipTrace).to({ y: traceTargetY, x: traceTargetX, alpha: .5 }, 2000, "Quad.easeInOut", true, 0, -1, true);

    }

    tweenShip({ level }) {
        this.shipTween.stop();
        this.shipTraceTween.stop();
        if (level == 1) {
            this.shipTraceTween = game.add.tween(this.shipTrace).to({ y: 300, x: -600 }, 1000, "Back.easeInOut", true, 0, 0, false);
            this.shipTween = game.add.tween(this.ship).to({ y: 200, x: -1000 }, 1000, "Back.easeInOut", true, 0, 0, false);
        } else if (level == 2) {
            this.shipTraceTween = game.add.tween(this.shipTrace).to({ y: 300, x: 2000 }, 1000, "Back.easeInOut", true, 0, 0, false);
            this.shipTween = game.add.tween(this.ship).to({ y: 0, x: 2800 }, 1000, "Back.easeInOut", true, 0, 0, false);

        }
    }

    selfieScreen() {

        this.selfieCharacter = new CharacterGroup({
            game: this.game,
            x: 0,
            y: 0,
            asset: this.game.globals.characterNames[this.game.globals.character],
            animate: true
        });
        this.selfieCharacter.width *= 2.2;
        this.selfieCharacter.height *= 2.2;
        this.selfieCharacter.y = 250 + (this.selfieCharacter.height / 2);
        this.selfieCharacter.x = this.selfieCharacter.width / 2;
        this.add(this.selfieCharacter);
        this.selfieCharacter.visible = false;

        this.selfiHeaderText = new Phaser.Text(this.game, 0, 160, 'PROTECTION ZONE CHAMPION', {
            font: '60px Slackey',
            fill: '#ffa201',
            align: 'center',
            smoothed: 'false'
        });
        this.selfiHeaderText.x = (this.game.width - this.selfiHeaderText.width) / 2;
        this.add(this.selfiHeaderText);
        this.selfiHeaderText.visible = false;

        this.selfieShareText = new Phaser.Text(this.game, 0, 255, 'Why not share a picture with your friends?', {
            font: '38px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        this.selfieShareText.x = (this.game.width - this.selfieShareText.width) / 2;
        this.add(this.selfieShareText);
        this.selfieShareText.visible = false;

        this.selfieTagText = new Phaser.Text(this.game, 0, 300, '#protectionzonechampion', {
            font: '38px Open Sans',
            fill: '#ffa201',
            align: 'center',
            smoothed: 'false'
        });
        this.selfieTagText.x = (this.game.width - this.selfieTagText.width) / 2;
        this.add(this.selfieTagText);
        this.selfieTagText.visible = false;

        this.selfiCongratulationsText = new Phaser.Text(this.game, 0, 400, 'CONGRATULATIONS!', {
            font: '80px Slackey',
            fill: '#53c7f1',
            align: 'center',
            smoothed: 'false'
        });
        this.selfiCongratulationsText.x = (this.game.width - this.selfiCongratulationsText.width) / 2;
        this.add(this.selfiCongratulationsText);
        this.selfiCongratulationsText.visible = false;

        this.yourScoreText = new Phaser.Text(this.game, 0, 525, 'Your score is!', {
            font: '46px Slackey',
            fill: '#fff1da',
            align: 'center',
            smoothed: 'false'
        });
        this.yourScoreText.x = (this.game.width - this.yourScoreText.width) / 2;
        this.add(this.yourScoreText);
        this.yourScoreText.visible = false;

        this.scoreText = new Phaser.Text(this.game, 0, 565, this.game.globals.score, {
            font: '144px Slackey',
            fill: '#fff1da',
            align: 'center',
            smoothed: 'false'
        });
        this.scoreText.x = (this.game.width - this.scoreText.width) / 2;
        this.add(this.scoreText);
        this.scoreText.visible = false;

        this.logos = this.create(0, 920, 'logoswhite');
        this.logos.x = (this.game.width - this.logos.width) / 2;
        this.logos.visible = false;

        this.selfieFooterText = new Phaser.Text(this.game, 0, this.game.height - 60, 'Date of preparation: May 2018 Job code: NP-4135, HEM-UK-6006', {
            font: '24px Open Sans',
            fill: '#FFFFFF',
            align: 'center',
            smoothed: 'false'
        });
        this.selfieFooterText.x = (this.game.width - this.selfieFooterText.width) / 2;
        this.add(this.selfieFooterText);
        this.selfieFooterText.visible = false;
    }

    levelComplete({ level }) {

        this.screen = 0;
        this.headerText.setText(`LEVEL ${level}\nCOMPLETE`);
        this.headerText.x = (this.game.width - this.headerText.width) / 2;
        

        if (level == 1) {
            this.wellDoneText.setText('Well done, you’ve defended your planet’s\nprotection zone from the first wave of aliens!')
        } else if (level == 2) {
            this.wellDoneText.setText('Great work, your active control of the spaceship\nhas protected your planet from aliens again!')
        } else if (level == 3) {
            this.wellDoneText.setText('Congratulations, you’ve successfully\nprotected your planet from aliens!')
        }
        this.wellDoneText.x = (this.game.width - this.wellDoneText.width) / 2;

         this.selectedCharacter.visible = true;
         this.headerText.visible = true;
         this.wellDoneText.visible = true;

        this.levelInfoText.visible = false;
        this.timerText.visible = false;
        this.animateLevelCompleteTexts();
    }

    animateLevelCompleteTexts() {
        this.readyForNext = false;

        this.selectedCharacter.alpha = 0;
        this.headerText.alpha = 0;
        this.wellDoneText.alpha = 0;
        this.selectedCharacter.alpha = 0;
        this.footerText.alpha = 0;
        this.moveAnim.alpha = 0;

        this.game.add.tween(this.selectedCharacter).to({ alpha: 1 }, 1000, "Linear", true, 0, 0, false);
        this.game.add.tween(this.headerText).to({ alpha: 1 }, 1500, "Linear", true, 1000, 0, false);
        this.game.add.tween(this.wellDoneText).to({ alpha: 1 }, 1500, "Linear", true, 2700, 0, false);
        this.game.add.tween(this.footerText).to({ alpha: 1 }, 1500, "Linear", true, 5000, 0, false);
        this.nextAnimation = this.game.add.tween(this.moveAnim).to({ alpha: 1 }, 1500, "Linear", true, 5000, 0, false);
        this.nextAnimation.onComplete.add(() => this.readyForNext = true, this);
    }

    levelInfo({ level }) {
        this.levelText.setText(`LEVEL ${level+1}`);
        this.levelText.x = (this.game.width - this.levelText.width) / 2;
        this.setPlanets({ level: level });
        if (level == 1) {
            this.levelInfoText.setText("Protection is essential for people\nwith haemophilia. However, although\ntreatment can protect them from bleeds,\nthat’s not all the protection they need.");
            this.startNewLevelText.setText("Let's see if you protect\nyour planet from the second wave...");            
        } else if (level == 2) {
            this.levelInfoText.setText("You’ve shown you know the basics\nof protection, but did you also know that\nmaintaining joint health in people\nwith haemophilia is a key goal?");
            this.startNewLevelText.setText("Time to get active again\nand see if you can protect your planet\nfrom the third and final wave...");
        } else if (level == 3) {
            this.levelInfoText.setText("You’re clearly pretty good at protection,\nbut did you know that true protection is about\nmore than just bleed prevention?\nIt’s about improving the\nquality of life of people with haemophilia.");
            this.startNewLevelText.setText("You can learn more about protection\nfor people with haemophilia by exploring\nthe rest of our booth or by asking\none of our friendly booth staff.");
        }

        this.animateLevelInfoTexts();
        this.levelInfoText.x = (this.game.width - this.levelInfoText.width) / 2;
        this.startNewLevelText.x = (this.game.width - this.startNewLevelText.width) / 2;
        this.selectedCharacter.visible = false;
        this.wellDoneText.visible = false;
        this.startNewLevelText.visible = true;
        this.headerText.visible = false;
        this.levelInfoText.visible = true;
        this.timerText.visible = false;
    }

    animateLevelInfoTexts() {
        this.readyForNext = false;
        this.levelInfoText.alpha = 0;
        this.startNewLevelText.alpha = 0;
        this.footerText.alpha = 0;
        this.moveAnim.alpha = 0;
        this.game.add.tween(this.levelInfoText).to({ alpha: 1 }, 1500, "Linear", true, 0, 0, false);
        this.game.add.tween(this.startNewLevelText).to({ alpha: 1 }, 1500, "Linear", true, 6000, 0, false);
        this.game.add.tween(this.footerText).to({ alpha: 1 }, 1500, "Linear", true, 9000, 0, false);
        this.nextAnimation = this.game.add.tween(this.moveAnim).to({ alpha: 1 }, 1500, "Linear", true, 9000, 0, false);
        this.nextAnimation.onComplete.add(() => this.readyForNext = true, this);
    }

    setSelfieScreen() {
        this.selectedCharacter.visible = false;
        this.headerText.visible = false;
        this.wellDoneText.visible = false;
        this.levelInfoText.visible = false;
        this.timerText.visible = false;
        this.footerText.visible = false;
        this.startNewLevelText.visible = false;
        this.moveAnim.visible = false;

        this.selfieCharacter.visible = true;
        this.selfieShareText.visible = false;
        this.selfiHeaderText.visible = true;
        this.selfieTagText.visible = false;
        this.selfiCongratulationsText.visible = true;
        this.yourScoreText.visible = true;
        this.scoreText.visible = true;
        this.logos.visible = true;
        this.selfieFooterText.visible = true;


        this.nextAnimation = this.game.add.tween(this).to({ alpha: 1 }, 15000, "Linear", true, 0, 0, false);
        this.nextAnimation.onComplete.add(() => {
            this.changeState = true;
        } , this);
        
    }

    changeLevel({ countdownTime }) {
        this.levelText.visible = true;
        this.missionText.visible = true;
        this.loading.visible = true;
        this.countdownTime = countdownTime;
        if (this.countdownTime > 0) {
            this.timerText.setText(this.countdownTime);
        } else {
            this.timerText.setText("GO!");
            this.loading.visible = false;
        }
        this.timerText.x = (this.game.width - this.timerText.width) / 2;

        this.levelInfoText.visible = false;
        this.timerText.visible = true;
        this.startNewLevelText.visible = false;
        this.moveAnim.visible = false;
        this.footerText.visible = false;

        this.ship.visible = true;
        this.shipTrace.visible = true;
        this.planetA.visible = true;
        this.planetB.visible = true;
        this.planetC.visible = true;
        this.planetD.visible = true;

    }

    setPlanets({ level }) {
        if (level == 0) {
            this.planetA = new Planet({
                game: this.game,
                x: 0,
                y: 0,
                asset: 'planetA',
                frameName: 'Planet_A_'
            });
            this.planetA.visible = false;
            this.planetB = new Planet({
                game: this.game,
                x: 0,
                y: 0,
                asset: 'planetB',
                frameName: 'Planet_B_'
            });
            this.planetC = new Planet({
                game: this.game,
                x: 0,
                y: 0,
                asset: 'planetC',
                frameName: 'Planet_C_'
            });
            this.planetD = new Planet({
                game: this.game,
                x: 0,
                y: 0,
                asset: 'planetD',
                frameName: 'Planet_D_'
            });

            this.planetA.visible = false;
            this.planetB.visible = false;
            this.planetC.visible = false;
            this.planetD.visible = false;

            this.add(this.planetA);
            this.add(this.planetB);
            this.add(this.planetC);
            this.add(this.planetD);
        } else if (level == 1) {

            this.planetA.x = 0;
            this.planetA.y = 200;
            this.planetA.width *= 1.9;
            this.planetA.height *= 1.9;
            this.planetA.angle = -55;


            this.planetB.x = -300;
            this.planetB.y = 650;
            this.planetB.width *= 3;
            this.planetB.height *= 3;

            this.planetC.x = 350;
            this.planetC.y = 350;
            this.planetC.width *= .8;
            this.planetC.height *= .8;

            this.planetD.x = 1850;
            this.planetD.y = 150;
            //this.planetD.width *= .7;
            //this.planetD.height *= .7;

        } else if (level == 2) {
            this.planetA.x = 1530;
            this.planetA.y = 80;
            this.planetA.width *= 1.2;
            this.planetA.height *= 1.2;

            this.planetB.x = 1300;
            this.planetB.y = 0;
            this.planetB.width *= .5;
            this.planetB.height *= .5;

            this.planetC.x = -30;
            this.planetC.y = 400;
            this.planetC.width *= 2;
            this.planetC.height *= 2;

            this.planetD.x = 150;
            this.planetD.y = 150;
            this.planetD.width *= .5;
            this.planetD.height *= .5;
        }
    }

}