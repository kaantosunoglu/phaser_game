import Phaser from 'phaser';
//import HoldAnimation from '../sprites/HoldAnimation';
import HoldAnimation from '../sprites/ButtonAnimation';

export default class extends Phaser.Group {
  constructor({ game, buttonText }) {
    super(game);
    let button = this.create(0, 0, 'choose');
    let buttonTexts = new Phaser.Text(this.game, 0, 0, buttonText, {
      font: '38px Slackey',
      fill: '#fff1da',
      align: 'center',
      smoothed: false
    });

    buttonTexts.setShadow(-3, 3, 'rgba(0,0,0,0.5)', 0);
    buttonTexts.x = (button.width - buttonTexts.width) / 2;
    buttonTexts.y = (button.height - buttonTexts.height) / 2;
    this.add(buttonTexts);

    this.animatedCursor = new HoldAnimation({
      game:this.game,
      x: 0,
      y: 0,
      asset: 'circleselected'
  })
  this.animatedCursor.width = button.width + 50;
  this.animatedCursor.height = button.height + 50;
  this.add(this.animatedCursor);
  this.animatedCursor.x = (button.x + (this.animatedCursor.width / 2)) - 25;
  this.animatedCursor.y = (button.y + (this.animatedCursor.height / 2)) - 25;
    
  }

  startAnimation() {
    this.animatedCursor.startAnimation();
  }

  stopAnimation() {
    this.animatedCursor.cancelAnimation();
  }
}
