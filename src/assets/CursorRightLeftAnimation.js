import Phaser from 'phaser';

export default class extends Phaser.Group {
    constructor({ game }) {
        super(game);
    
        this.linesMove = this.create(0, 8, 'linesleftright');
        this.cursor.anchor.setTo (0.5, .5);
        this.cursor = this.create(0, 0, 'cursor');
       // this.cursor.width /= 2;
       // this.cursor.height /= 2;
        this.cursor.anchor.setTo (0.5, .5);
        this.cursor.x = -30;
        game.add.tween(this.cursor).to({ x: 30 }, 1000, "Quad.easeInOut", true, 0, -1, true);
    }
}