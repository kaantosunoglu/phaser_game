import Phaser from 'phaser'

export default class extends Phaser.Sprite {
  constructor({ game, x, y, asset }) {
    //super(game, type)
    super(game, x, y, asset);
    this.spriteC = null;
    this.listenLeapController();

  }

  listenLeapController() {
    var _root = this;
    var firstClose = true;
    var took = false;
    //var swipeDirection = "";
    Leap.loop({ enableGestures: true }, function (frame) {

      game.globals.handCount = frame.hands.length;
      if (frame.hands.length > 0) {
        // frame.hands.forEach(function (hand, index) {
        game.globals.handX = frame.hands[0].screenPosition()[0];
        game.globals.handY = frame.hands[0].screenPosition()[2];

        game.globals.grabStrength = frame.hands[0].grabStrength;

        if (frame.valid && (frame.gestures.length > 0 || frame.gestures.speed > 0)) {
          
          frame.gestures.forEach(function (gesture) {
         //   console.log("gesture.type  " + gesture.type);
            switch (gesture.type) {
              case "circle":
                game.globals.swipeDirection = "wave";
                break;
              case "swipe":
                var isHorizontal = Math.abs(gesture.direction[0]) > Math.abs(gesture.direction[1]);
                if (isHorizontal) {
                  if (gesture.direction[0] > 0 && gesture.speed > 220) {
                    game.globals.swipeDirection = "right";
                  } else if (gesture.direction[0] < 0 && gesture.speed > 220) {
                    game.globals.swipeDirection = "left";
                  } else if (gesture.speed > 50) {
                    game.globals.swipeDirection = "wave";
                  } else if (gesture.speed < 50) {
                    game.globals.swipeDirection = "hold";
                  }
                }
               // console.log("=========>   " + game.globals.swipeDirection);
                //   console.log("Swipe Gesture : " + game.globals.swipeDirection + "  " + (Math.floor(gesture.speed)));
                break;
              


            }
          });
        } else {
       //  console.log("frame gesture length <= 0");
          game.globals.swipeDirection = "hold"
        }

      } else {
        game.globals.swipeDirection = "";
      }
      // });

    }).use('screenPosition', { scale: 1 });
  }
}