import Phaser from 'phaser';

export default class extends Phaser.Sprite {
    constructor({ game, x, y, asset }) {
        super(game, x, y, asset);
        this.width = 175;
        this.height = 175;
        this.anchor.setTo(.5);
    }
    startAnimation() {
        this.animations.add('hold', Phaser.Animation.generateFrameNames('Circle_Selected', 0, 29, '', 5), 12, false);
        this.animations.play('hold');
      }
    
      cancelAnimation() {
        this.animations.stop(null, true);
      }

      loopAnimation() {
        this.animations.add('hold', Phaser.Animation.generateFrameNames('Circle_Selected', 0, 29, '', 5), 12, true);
        this.animations.play('hold');          
      }
    
}