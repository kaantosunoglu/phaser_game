import Phaser from 'phaser';
import Character from '../sprites/Character'

export default class extends Phaser.Group {
    constructor({ game, CharacterName }) {
        super(game);
        this.game = game;
        this.CharacterName = CharacterName;
        this.createCharacter();
    }

    createCharacter() {
        this.characterSelected = new Character({
            game: this.game,
            x: 0,
            y: 0,
            asset: this.CharacterName + 'Selected',
            animate: false
        })
        this.add(this.characterSelected);
        this.characterSelected.alpha = 0;
        this.character = new Character({
            game: this.game,
            x: 0,
            y: 0,
            asset: this.CharacterName,
            animate: false
        })
        this.add(this.character);

        this.CharacterText = new Phaser.Text(this.game, 0, 550, this.CharacterName, {
            font: '66px Slackey',
            fill: '#fff1da',
            smoothed: true,
            align: 'center'
        });

        this.CharacterText.x -= (this.CharacterText.width) / 2;
        this.add(this.CharacterText);
        this.character.y = this.height / 2;
        this.onRolledOver = false;

        this.characterSelected.height = this.character.height + 58;
        this.characterSelected.width = this.character.width;
        this.characterSelected.x = this.character.x;
        this.characterSelected.y = this.character.y;

        this.character.width *= 1.65;
        this.character.height *= 1.65;
        this.characterSelected.width *= 1.65;
        this.characterSelected.height *= 1.65;

        this.game.time.events.add(Phaser.Timer.SECOND * (Math.random() * 5), this.playAnimation, this);
    }

    playAnimation() {
        this.character.playAnimation();
        this.characterSelected.playAnimation();
        this.game.time.events.add(Phaser.Timer.SECOND * (Math.random() * 30), this.playAnimation, this);
    }

    onRollOver() {
        this.characterSelected.alpha = 1;
    }
    onRollOut() {
        this.characterSelected.alpha = 0;
    }
}  