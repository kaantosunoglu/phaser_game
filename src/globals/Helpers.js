export default class {
    checkOverlap(spriteA, spriteB) {

        var boundsA = spriteA.getBounds();
        var boundsB = spriteB.getBounds();
       
        return Phaser.Rectangle.intersects(boundsA, boundsB);

    }

    setShipAngle(shipPosition, lastPosition, spaceShip) {

        this.ship = spaceShip;

        // I just want to give ship movement effect smootly
        if (shipPosition - lastPosition >= -.5 && shipPosition - lastPosition <= .5) {
          if (this.ship.angle >= .5 && this.ship.angle <= .5) {
            this.ship.angle = 0;
          } else {
            this.ship.angle += (this.ship.angle / -5);
          }
        } else if (shipPosition - lastPosition < -.5) {
          if (this.ship.angle > 0) {
            this.ship.angle += (this.ship.angle < .4) ? (this.ship.angle / -1) : (this.ship.angle / -5);
          } else {
            this.ship.angle += (-20 < this.ship.angle) ? -.4 : 0;
          }
        } else {
          if (this.ship.angle < 0) {
            this.ship.angle += (this.ship.angle > -0.4) ? (this.ship.angle / -1) : (this.ship.angle / -5);
          } else {
            this.ship.angle += (20 > this.ship.angle) ? .4 : 0;
          }
        }
      }
}